import NeutrinoEffect from 'NeutrinoEffect_water_stream';

// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    //@property(cc.Label)
    //label: cc.Label = null;

    //@property
    //text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    colors = [new cc.Vec3(1, 0, 0), new cc.Vec3(0, 1, 0), new cc.Vec3(0, 0, 1)];
    currentColor = 0;

    start () {
    }

    // update (dt) {}

    createStream() {
        this.node.removeComponent(NeutrinoEffect);
        const effect = this.node.addComponent(NeutrinoEffect);
        effect.pausedOnStart = true;
    }

    unpauseStream() {
        const effect = this.getComponent(NeutrinoEffect);
        if (!effect) {
            return;
        }

        effect.unpause();
    }

    colorStream() {
        const effect = this.getComponent(NeutrinoEffect);
        if (!effect) {
            return;
        }

        effect.setPropertyInAllEmitters('Color', this.colors[this.currentColor]);
        this.currentColor = (this.currentColor + 1) % this.colors.length;
    }
}
