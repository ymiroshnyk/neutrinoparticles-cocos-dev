// 68e2103e-fab2-4c7a-a887-182172099916


(function (root, factory) {
    if (typeof exports === 'object' && typeof module !== 'undefined') {
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) {
        define(['exports'], function (exports) {
            (root.NeutrinoEffect = exports)['splashGlimmerL'] = factory();
        });
    } else {
        var namespace = (root.NeutrinoEffect || (root.NeutrinoEffect = {}));
        namespace.__last__ = namespace['splashGlimmerL'] = factory();
    }
}(typeof self !== 'undefined' ? self : this, function () {

function splashGlimmerL(ctx) {
	var Db = this;

	var ne = function (Ld, Bd) {
		this.Ld = Ld;
		this.Bd = Bd;

		if (this.Bd.we.pe.length > 0) {
			this.we = this.Bd.we.pe[0];

			this.Lc = [ne.prototype.Ec,
				ne.prototype.Fc][this.we.xe];
		}
		else
			this.we = null;
	}

	ne.prototype = {
		Ec: function (fe, Ab, Xb) {
			var Gc = ctx.ib(Xb.Md);
			var Hc = Math.cos(Gc);
			var Ic = Math.sin(Gc);
			var ye = ctx.Ae(Xb.Nd[0]);
			var ze = ctx.Ae(Xb.Nd[1]);
			fe./**/transform(ye * Hc, ye * Ic, ze * -Ic, ze * Hc, Ab[0], Ab[1]);
		},

		Fc: function (fe, Ab, Xb) {
			var q = Xb.Mc;
			var z2 = 2.0 * q[2] * q[2];
			var xy = 2.0 * q[0] * q[1];
			var wz = 2.0 * q[3] * q[2];
			var ye = ctx.Ae(Xb.Nd[0]);
			var ze = ctx.Ae(Xb.Nd[1]);
			fe./**/transform(
				ye * (1.0 - 2.0 * q[1] * q[1] - z2),
				ye * (xy + wz),
				ze * (wz - xy),
				ze * (2.0 * q[0] * q[0] + z2 - 1.0),
				Ab[0], Ab[1]);
		},

		Pc: function (fe, Xb, ge) {
			Xb.vc(fe, -1, ge);

			if (this.we) {

				if (this.Be != null && !Xb.oc) {

					if (Xb.Od > 0.001) {
						var De = Math.floor(Xb.Qc % this.we.Rc);
						var Ee = Math.floor(Xb.Qc / this.we.Rc);

						var Ab = Xb.Ab.slice();
						var Nd = Xb.Nd.slice();
						if (!ge || ge./**/transform(Ab, Nd)) {

							var df = Math.abs(Nd[0]);
							var ef = Math.abs(Nd[1]);

							if (df > 0.001 && ef > 0.001) {
								fe.save();
								this.Lc(fe, Ab, Xb);

								fe.translate(-df * Xb.Pd[0], -ef * (1 - Xb.Pd[1]));
								fe.globalAlpha = Xb.Od;

								if (Xb.gf[0] < 0.999 || Xb.gf[1] < 0.999 || Xb.gf[2] < 0.999) {
									if (df >= 1 && ef >= 1) {
										var Ye = df < this.Tc ? df : this.Tc;
										var Ze = ef < this.Uc ? ef : this.Uc;

										ctx.af(Ye, Ze);

										ctx.bf.globalCompositeOperation = "copy";
										ctx.bf.drawImage(this.Be.image,
											this.Be.x + this.Tc * De, this.Be.y + this.Uc * Ee,
											this.Tc, this.Uc,
											0, 0, Ye, Ze);

										ctx.bf.globalCompositeOperation = "multiply";
										ctx.bf.fillStyle = ctx.ff(Xb.gf);
										ctx.bf.fillRect(0, 0, Ye, Ze);

										ctx.bf.globalCompositeOperation = "destination-atop";
										ctx.bf.drawImage(this.Be.image,
											this.Be.x + this.Tc * De, this.Be.y + this.Uc * Ee,
											this.Tc, this.Uc,
											0, 0, Ye, Ze);

										fe.drawImage(ctx.cf, 0, 0, Ye, Ze, 0, 0, df, ef);
									}
								}
								else {
									fe.drawImage(this.Be.image,
										this.Be.x + this.Tc * De, this.Be.y + this.Uc * Ee,
										this.Tc, this.Uc, 0, 0, df, ef);
								}

								fe.restore();
							}
						}
					}
				}
			}

			Xb.vc(fe, 1, ge);
		},

		Hd: function (fe, ge) {
			fe.save();

			if (this.we) {
				fe.globalCompositeOperation = this.Ld.materials[this.Ld./**/model.renderStyles[this.we.renderStyleIndex].materialIndex];
				this.Be = this.Ld.textureDescs[this.Ld./**/model.renderStyles[this.we.renderStyleIndex].textureIndices[0]];
			}
			else {
				this.Be = null;
			}

			if (this.Be) {
				this.Tc = this.Be.width / this.we.Rc;
				this.Uc = this.Be.height / this.we.Sc;
			}

			function kd(a, b) {
				if (a.Ab[2] < b.Ab[2])
					return 1;
				if (a.Ab[2] > b.Ab[2])
					return -1;
				return 0;
			}

			switch (this.Bd.Vc) {
				case 0:
					for (var Wb = 0; Wb < this.Bd.tc.length; ++Wb) {
						this.Pc(fe, this.Bd.tc[Wb], ge);
					}
					break;
				case 1:
					for (var Wb = this.Bd.tc.length; Wb-- > 0;) {
						this.Pc(fe, this.Bd.tc[Wb], ge);
					}
					break;
				case 2:
					this.Bd.tc.sort(kd);

					for (var Wb = 0; Wb < this.Bd.tc.length; ++Wb) {
						this.Pc(fe, this.Bd.tc[Wb], ge);
					}
					break;
			}

			fe.restore();
		}
	}

	var oe = function (Ld, Bd) {

		this.Ld = Ld;
		this.Bd = Bd;

		if (this.Bd.we.pe.length > 0)
			this.we = this.Bd.we.pe[0];
		else
			this.we = null;

		this.vertex = [
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] },
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] },
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] },
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] }];
	}

	oe.prototype = {
		qe: function (Xb, se, re, te, renderBuffer) {
			Xb.Ce(-1, se, re, te, renderBuffer);

			if (this.we) {

				if (!Xb.oc) {

					var v0 = this.vertex[0];
					var v1 = this.vertex[1];
					var v2 = this.vertex[2];
					var v3 = this.vertex[3];

					var Fe = [], Ge = [];

					if (this.we.xe == 0) {
						var a = ctx.ib(Xb.Md);
						var s = -Math.sin(a);
						var c = Math.cos(a);

						Fe[0] = se[0] * c + re[0] * s;
						Fe[1] = se[1] * c + re[1] * s;
						Fe[2] = se[2] * c + re[2] * s;

						Ge[0] = -se[0] * s + re[0] * c;
						Ge[1] = -se[1] * s + re[1] * c;
						Ge[2] = -se[2] * s + re[2] * c;
					}
					else {
						var q = Xb.Mc;
						var z2 = 2.0 * q[2] * q[2];
						var xy = 2.0 * q[0] * q[1];
						var wz = 2.0 * q[3] * q[2];

						Fe[0] = 1.0 - 2.0 * q[1] * q[1] - z2;
						Fe[1] = xy + wz;
						Fe[2] = 2.0 * q[0] * q[2] - 2.0 * q[3] * q[1];

						Ge[0] = xy - wz;
						Ge[1] = 1.0 - 2.0 * q[0] * q[0] - z2;
						Ge[2] = 2.0 * q[1] * q[2] + 2.0 * q[3] * q[0];
					}

					var He = [], Ie = [], Je = [], Ke = [];
					ctx.u(He, Fe, -Xb.Nd[0] * Xb.Pd[0]);
					ctx.u(Ie, Fe, Xb.Nd[0] * (1.0 - Xb.Pd[0]));
					ctx.u(Je, Ge, -Xb.Nd[1] * Xb.Pd[1]);
					ctx.u(Ke, Ge, Xb.Nd[1] * (1.0 - Xb.Pd[1]));

					ctx.c(v0./**/position, He, Je);
					ctx.c(v0./**/position, v0./**/position, Xb.Ab);
					ctx.c(v1./**/position, He, Ke);
					ctx.c(v1./**/position, v1./**/position, Xb.Ab);
					ctx.c(v2./**/position, Ie, Ke);
					ctx.c(v2./**/position, v2./**/position, Xb.Ab);
					ctx.c(v3./**/position, Ie, Je);
					ctx.c(v3./**/position, v3./**/position, Xb.Ab);

					{
						var rgb = ctx.v(Xb.gf, 255);
						v0./**/color = v1./**/color = v2./**/color = v3./**/color = [rgb[0], rgb[1], rgb[2], Xb.Od * 255];
					}

					{
						var De = Math.floor(Xb.Qc % this.we.Rc);
						var Ee = Math.floor(Xb.Qc / this.we.Rc);

						var Pe, Qe, Re, Se;

						var We = this.Ld.texturesRemap[this.Ld./**/model.renderStyles[this.we.renderStyleIndex].textureIndices[0]];
						if (We) {
							var Ue = We.width / this.we.Rc;
							var Ve = We.height / this.we.Sc;

							var Pe = We.x + De * Ue;
							var Qe = Pe + Ue;
							var Re = (We.y + We.height - Ee * Ve);
							var Se = Re - Ve;
						} else {
							var Ue = 1.0 / this.we.Rc;
							var Ve = 1.0 / this.we.Sc;

							var Pe = De * Ue;
							var Qe = Pe + Ue;
							var Re = (1.0 - Ee * Ve);
							var Se = Re - Ve;
						}

						v0./**/texCoords[0] = [Pe, Se];
						v1./**/texCoords[0] = [Pe, Re];
						v2./**/texCoords[0] = [Qe, Re];
						v3./**/texCoords[0] = [Qe, Se];
					}

					if (renderBuffer.beforeQuad) {
						renderBuffer.beforeQuad(this.we.renderStyleIndex);
					}

					renderBuffer.pushVertex(v0);
					renderBuffer.pushVertex(v1);
					renderBuffer.pushVertex(v2);
					renderBuffer.pushVertex(v3);

					if (!renderBuffer.__lastRenderCall) {
						renderBuffer.__lastRenderCall = new ctx.RenderCall(0, 6, this.we.renderStyleIndex);
					} else {
						var lastRenderCall = renderBuffer.__lastRenderCall;

						if (lastRenderCall.renderStyleIndex == this.we.renderStyleIndex) {
							lastRenderCall.numIndices += 6;
						} else {
							renderBuffer.pushRenderCall(lastRenderCall);
							renderBuffer.__lastRenderCall = new ctx.RenderCall(
								lastRenderCall.startIndex + lastRenderCall.numIndices,
								6, this.we.renderStyleIndex);
						}
					}
				}
			}

			Xb.Ce(1, se, re, te, renderBuffer);
		},

		ue: function (se, re, te, renderBuffer) {
			switch (this.Bd.Vc) {
				case 0:
					for (var Wb = 0; Wb < this.Bd.tc.length; ++Wb) {
						this.qe(this.Bd.tc[Wb], se, re, te, renderBuffer);
					}
					break;

				case 1:
					for (var Wb = this.Bd.tc.length; Wb-- > 0;) {
						this.qe(this.Bd.tc[Wb], se, re, te, renderBuffer);
					}
					break;

				case 2:
					this.Bd.tc.forEach(function (Xb) {
						Xb.depth = ctx.H(te, Xb.Ab);
					});

					this.Bd.tc.sort(function (a, b) {
						if (a.depth < b.depth)
							return 1;
						if (a.depth > b.depth)
							return -1;
						return 0;
					});

					this.Bd.tc.forEach(function (Xb) {
						this.qe(Xb, se, re, te, renderBuffer);
					}, this);
					break;
			}
		}
	}

	var ld = function (Ld, we, ve) {
		var Vb = this;
		this.Ld = Ld;
		this.we = we;

		// Eb

		function Eb() {
			this.Fb = 0;
			this.Gb = 1;
			this.Hb = null;
			this.Ib = null;
			this.Kb = 0;
			this.Lb = 1;

			Vb.we.Mb(this); // IMPL

			this.Nb = function () {
				this.Ob = this.Gb;
				this.Fb = 0;
			}

			this.Nb();
		}

		Eb.prototype = {
			Jd: function () {
				this.Nb();
			},

			Id: function (Qb, Ab, Mc) {
				Vb.we.Pb(Qb, Vb, this); // IMPL

				var Rb = Vb.Rb;
				var systemTime = Ld.Rb;
				var Sb = Qb;
				var ic = 0;

				if (this.zb > 0.000001) {

					var Tb = this.Ob + Qb * this.zb;

					while (Tb >= 1.0) {
						var Ub = this.zb < 0.001 ? 0.0 : (1.0 - this.Ob) / this.zb;
						Sb -= Ub;
						Rb += Ub;
						systemTime += Ub;

						if (this.Hb != null && Rb > this.Hb) {
							Vb.disactivate();
							break;
						}

						Vb.Rb = Rb;
						Ld.Rb = systemTime;

						if (Ab && Qb > 0)
							ctx.ab(Vb.Ab, Ab, Vb.Bb, Sb / Qb);

						if (Mc && Qb > 0)
							ctx.slerpq(Vb.Mc, Mc, Vb.prevRotation, Sb / Qb);

						// for the future when Jb would be external
						this.Lb = this.Jb;

						for (var Wb = 0; Wb < this.Jb; ++Wb) {
							if (Vb.sc.length == 0)
								break;

							if (this.Jb == 1)
								this.Kb = 0;
							else
								this.Kb = Wb / (this.Jb - 1);

							var Xb = Vb.sc.pop();
							Vb.tc.unshift(Xb);

							if (Wb == 0)
								Xb.Yb();
							else
								Xb.Zb();

							Xb.Id(Sb);
							++ic;
						}

						this.Ob = 0.0;
						Tb -= 1.0;

						if (this.Ib != null && ++this.Fb >= this.Ib) {
							Vb.disactivate();
							break;
						}
					}

					this.Ob = Tb;
				}
				Rb += Sb;
				Vb.Rb = Rb;

				if (Ab)
					ctx.T(Vb.Ab, Ab);

				if (Mc)
					ctx.U(Vb.Mc, Mc);

				return ic;
			}
		}

		// ac

		function ac() {
			this.Gb = 1;
			this.Kb = 0;
			this.Lb = 1;

			Vb.we.Mb(this); // IMPL

			this.Nb = function () {
				this.bc = this.Gb;
			}

			this.Nb();
		}

		ac.prototype = {
			Jd: function () {
				this.Nb();
			},

			Id: function (Qb, Ab, Mc) {
				Vb.we.Pb(Qb, Vb, this); // IMPL

				var cc = Vb.Rb;
				var dc = cc + Qb;
				var systemTimeBeforeFrame = Ld.Rb;
				var systemTimeAfterFrame = systemTimeBeforeFrame + Qb;
				var ec = Ab ? ctx.O(ctx.h(Ab, Vb.Bb)) : 0;
				var ic = 0;

				if (ec > 0.000001) {
					var fc = ec / this.rd;
					var Tb = this.bc + fc;

					var hc = fc < 0.001 ?
						1.0 - this.bc : (1.0 - this.bc) / fc;

					var jc = [];

					while (Tb > 1.0) {
						var kc = cc + hc * Qb;

						if (Ab)
							ctx.ab(jc, Vb.Bb, Ab, hc);

						Vb.Rb = kc;
						ctx.T(Vb.Ab, jc);
						Ld.Rb = ctx.X(systemTimeBeforeFrame, systemTimeAfterFrame, hc);

						// for the future when Jb would be external
						this.Lb = this.Jb;

						for (var Wb = 0; Wb < this.Jb; ++Wb) {
							if (Vb.sc.length == 0)
								break;

							if (this.Jb == 1)
								this.Kb = 0;
							else
								this.Kb = Wb / (this.Jb - 1);

							var Xb = Vb.sc.pop();
							Vb.tc.unshift(Xb);

							if (Wb == 0)
								Xb.Yb();
							else
								Xb.Zb();

							Xb.Id(Qb * (1.0 - hc));
							++ic;
						}

						hc += 1.0 / fc;
						Tb -= 1.0;
					}

					this.bc = Tb;
				}

				Vb.Rb = dc;

				if (Ab)
					ctx.T(Vb.Ab, Ab);

				if (Mc)
					ctx.U(Vb.Mc, Mc);

				return ic;
			}
		}

		// mc

		function mc() {
			this.Ab = [];
			this.Pd = [];
			this.Nd = [];
			this.gf = [];
			this.Kc = [];
		}

		mc.prototype = {
			nc: function () {
				this.oc = false;

				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];
					pc.Bd.Jd(this.Ab, null);

					if (pc.Ad.sd)
						pc.Bd.disactivate();
				}
			},

			Yb: function () {
				Vb.we.fd(Vb, this); // IMPL
				this.nc();
			},

			Zb: function () {
				Vb.we.gd(Vb, this); // IMPL
				this.nc();
			},

			Id: function (Qb) {
				Vb.we.qc(Qb, Vb, this); // IMPL

				this.rc(Qb);
			},

			pc: function (je) {
				return this.Kc[je].Bd;
			},

			rc: function (Qb) {
				for (var i = 0; i < this.Kc.length; i++) {
					this.Kc[i].Bd.Id(Qb, this.Ab, null);
				}
			},

			uc: function (md, nd) {
				this.Kc.push({
					Bd: new ld(Ld, md, ve),
					Ad: nd
				});
			},

			vc: function (fe, xc, ge) {
				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];

					if (xc == pc.Ad.xc)
						pc.Bd.Hd(fe, ge);
				}
			},

			Ce: function (xc, se, re, te, renderBuffer) {
				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];

					if (xc == pc.Ad.xc)
						pc.Bd.ue(se, re, te, renderBuffer);
				}
			},

			wc: function (fe) {
				this.oc = true;
				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];

					if (pc.Ad.sd)
						pc.Bd.activate();
					else
						pc.Bd.disactivate();
				}
			},

			yc: function (Gd) {
				for (var i = 0; i < this.Kc.length; ++i) {
					this.Kc[i].Bd.Ed(Gd);
				}
			}
		}

		// zc

		function zc() {
		}

		zc.prototype.Ac = function (Xb) {
			return Vb.we.Cc(Vb, Xb, this); // IMPL
		}

		// ld Ad

		this.Ab = [];
		this.Bb = [];
		this.Mc = [];
		this.prevRotation = [];
		this.tc = [];
		this.sc = [];
		this.Wc = new zc();
		this.construct = new ve(this.Ld, this);
		this.Yc = [];
		this.ad = [];

		this.dd = function () {
			this.vd = new Eb();
		}

		this.ed = function () {
			this.vd = new ac();
		}

		this.we.ud(this); // IMPL

		for (var Wb = 0; Wb < this.jd; ++Wb) {
			var Xb = new mc();

			for (var id = 0; id < this.Yc.length; ++id) {
				var hd = this.Yc[id];
				Xb.uc(hd.Db, hd.Ad);
			}

			this.sc.push(Xb);
		}

		this.Nb = function (Ab, Mc) {

			ctx.T(this.Ab, Ab ? Ab : [0, 0, 0]);
			ctx.T(this.Bb, this.Ab);
			ctx.U(this.Mc, Mc ? Mc : [0, 0, 0, 1]);
			ctx.U(this.prevRotation, this.Mc);

			this.Rb = 0.0;
			this.wd = 0.0;
			this.Zc = true;
			this.paused_ = false;
			this.generatorsPaused_ = false;
			ctx.W(this.ad, 0, 0, 0);
		}
	}

	ld.prototype.Jd = function (Ab, Mc) {
		this.Nb(Ab, Mc);

		this.sc.push.apply(this.sc, this.tc);
		this.tc.length = 0;

		this.vd.Jd();
	}

	ld.prototype.Id = function (Qb, Ab, Mc) {

		if (this.paused_)
		{
			this.Td(Ab, Mc);
			return;
		}

		this.wd = this.Rb;

		if (Ab) {
			ctx.T(this.Bb, this.Ab);
			if (Qb > 0.0001) {
				var shift = [];
				ctx.g(shift, Ab, this.Bb);
				ctx.T(this.ad, shift);
				ctx.w(this.ad, this.ad, Qb);
			}
			else {
				ctx.W(this.ad, 0, 0, 0);
			}
		}
		else {
			ctx.W(this.ad, 0, 0, 0);
		}

		if (Mc)
		{
			ctx.U(this.prevRotation, this.Mc);
		}

		var ic;

		if (this.Zc && !this.generatorsPaused_) {
			ic = this.vd.Id(Qb, Ab, Mc);
		}
		else {
			if (Ab)
				ctx.T(this.Ab, Ab);

			if (Mc)
				ctx.U(this.Mc, Mc);

			ic = 0;
			this.Rb += Qb;
		}

		for (var Wb = ic; Wb < this.tc.length;) {
			var Xb = this.tc[Wb];

			if (!Xb.oc) {
				Xb.Id(Qb);

				if (this.Wc.Ac(this.tc[Wb])) {
					Xb.wc();

					if (this.xd(Wb))
						continue;
				}
			}
			else {
				Xb.rc(Qb);

				if (this.xd(Wb))
					continue;
			}

			++Wb;
		}
	};

	ld.prototype.xd = function (je) {
		var Xb = this.tc[je];

		var ready = true;

		for (var id = 0; id < Xb.Kc.length; ++id) {
			var Bd = Xb.Kc[id].Bd;

			if (Bd.activated() || Bd.tc.length > 0) {
				ready = false;
				break;
			}
		}

		if (ready) {
			this.sc.push(this.tc[je]);
			this.tc.splice(je, 1);
			return true;
		}

		return false;
	}

	ld.prototype.Hd = function (fe, ge) {
		this.construct.Hd(fe, ge);
	}

	ld.prototype.ue = function (se, re, te, renderBuffer) {
		this.construct.ue(se, re, te, renderBuffer);
	}

	ld.prototype.Td = function (Ab, Mc) {
		this.wd = this.Rb;

		if (Ab) {
			ctx.T(this.Bb, this.Ab);
			ctx.T(this.Ab, Ab);
		}

		if (Mc) {
			ctx.U(this.prevRotation, this.Mc);
			ctx.U(this.Mc, Mc);
		}
	}

	ld.prototype.uc = function (md, nd) {
		this.Yc.push({ Db: md, Ad: nd });
	}

	ld.prototype./**/pause = function () {
		this.paused_ = true;
	}

	ld.prototype./**/unpause = function () {
		this.paused_ = false;
	}

	ld.prototype./**/paused = function () {
		return this.paused_;
	}

	ld.prototype./**/pauseGenerators = function () {
		this.generatorsPaused_ = true;
	}

	ld.prototype./**/unpauseGenerators = function () {
		this.generatorsPaused_ = false;
	}

	ld.prototype./**/generatorsPaused = function () {
		return this.generatorsPaused_;
	}

	ld.prototype.activate = function () {
		this.Zc = true;
	}

	ld.prototype.disactivate = function () {
		this.Zc = false;
	}

	ld.prototype.activated = function () {
		return this.Zc;
	}

	ld.prototype./**/getNumParticles = function () {
		return this.tc.length;
	}

	var ke = function () {
		var Cb = this;

		this._init = function (we, Ab, Mc, ve, options) {
			this./**/model = we;

			this.Ab = [];
			this.Mc = [];

			// ke Ad

			this.od = [];

			this.pd = function (md) {
				var Bd = new ld(this, md, ve);
				Bd.Nb(this.Ab, this.Mc);
				this["_".concat(md.name)] = Bd;
				this.od.push(Bd);
			}

			this.Nb = function (Ab, Mc) {
				this.Cd = 0.0;
				this.Rb = 0.0;
				ctx.T(this.Ab, Ab ? Ab : [0, 0, 0]);
				ctx.U(this.Mc, Mc ? Mc : [0, 0, 0, 1]);
			}

			this.Nb(Ab, Mc);
			this./**/model.qd(this); // IMPL

			this._presimNeeded = true;

			if (options.paused) {
				this./**/pauseAllEmitters();
			} else {
				this.zeroUpdate();
				this./**/update(this.Ud, Ab, Mc);
				this._presimNeeded = false;
			}

			if (options.generatorsPaused) {
				this./**/pauseGeneratorsInAllEmitters();
			}
		}
	}

	ke.prototype./**/restart = function (/**/position, /**/rotation) {

		this.Nb(/**/position ? /**/position : this.Ab, /**/rotation ? /**/rotation : this.Mc);

		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Jd(this.Ab, this.Mc);
		}

		this.zeroUpdate();

		this./**/update(this.Ud, this.Ab, this.Mc);
		this._presimNeeded = false;
	}

	ke.prototype.zeroUpdate = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Id(0, this.Ab, this.Mc);
		}
	}

	ke.prototype./**/update = function (/**/dt, /**/position, /**/rotation) {
		var updatedTime = 0.0;
		var hc = [];
		ctx.T(hc, this.Ab);
		var frameRotation = [];
		ctx.U(frameRotation, this.Mc);

		if (/**/position && ctx.equalv3_(/**/position, this.Ab))
			/**/position = null;

		if (/**/rotation && ctx.equalq_(/**/rotation, this.Mc))
			/**/rotation = null;

		while ((/**/dt - updatedTime) + this.Cd >= this.Dd) {
			var cc = this.Rb;

			if (/**/position)
				ctx.ab(hc, this.Ab, /**/position, updatedTime / /**/dt);

			if (/**/rotation)
				ctx.slerpq(frameRotation, this.Mc, /**/rotation, updatedTime / /**/dt);

			for (var i = 0; i < this.od.length; ++i) {
				this.od[i].Id(this.Dd, hc, frameRotation);

				this.Rb = cc;
			}

			updatedTime += this.Dd - this.Cd;
			this.Cd = 0.0;
			this.Rb = cc + this.Dd;
		}

		if (/**/position)
			ctx.T(this.Ab, /**/position);

		if (/**/rotation)
			ctx.U(this.Mc, /**/rotation);

		this.Cd += /**/dt - updatedTime;
	}

	ke.prototype./**/resetPosition = function (/**/position, /**/rotation) {

		if (/**/position)
			ctx.T(this.Ab, /**/position);

		if (/**/rotation)
			ctx.U(this.Mc, /**/rotation);

		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Td(this.Ab, this.Mc);
		}
	}

	ke.prototype./**/setPropertyInAllEmitters = function (/**/name, /**/value) {
		var propName = "_".concat(/**/name);

		if (/**/value instanceof Array) {
			if (/**/value.length == 2) {
				for (var i = 0; i < this.od.length; ++i) {
					ctx.S(this.od[i][propName], /**/value);
				}
			}
			else {
				for (var i = 0; i < this.od.length; ++i) {
					ctx.T(this.od[i][propName], /**/value);
				}
			}
		}
		else {
			for (var i = 0; i < this.od.length; ++i) {
				this.od[i][propName] = /**/value;
			}
		}
	}

	ke.prototype./**/pauseAllEmitters = function() {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/pause();
		}
	}

	ke.prototype./**/unpauseAllEmitters = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/unpause();
		}
		this.zeroUpdate();

		if (this._presimNeeded) {
			this./**/update(this.Ud, this.Ab, this.Mc);
			this._presimNeeded = false;
		}
	}

	ke.prototype./**/areAllEmittersPaused = function () {
		for (var i = 0; i < this.od.length; ++i) {
			if (!this.od[i].paused())
				return false;
		}
		return true;
	}

	ke.prototype./**/pauseGeneratorsInAllEmitters = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/pauseGenerators();
		}
	}

	ke.prototype./**/unpauseGeneratorsInAllEmitters = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/unpauseGenerators();
		}
	}

	ke.prototype./**/areGeneratorsInAllEmittersPaused = function () {
		for (var i = 0; i < this.od.length; ++i) {
			if (!this.od[i].generatorsPaused())
				return false;
		}
		return true;
	}

	ke.prototype./**/getNumParticles = function() {
		var numParticles = 0;

		for (var i = 0; i < this.od.length; ++i) {
			numParticles += this.od[i].getNumParticles();
		}

		return numParticles;
	}


	var le = function () {
		this._init = function (we, Ab, Mc, renderBuffer, options) {
			le.prototype._init.call(this, we, Ab, Mc, oe, options);

			this.texturesRemap = [];

			var indices = [];

			{
				var verDisp;
				for (var Wb = 0; Wb < this./**/model.Xe; ++Wb) {
					verDisp = Wb * 4;
					indices.push(verDisp + 0, verDisp + 3, verDisp + 1, verDisp + 1, verDisp + 3, verDisp + 2);
				}
			}

			this.renderBuffer = renderBuffer;
			this.renderBuffer.initialize(this./**/model.Xe * 4, [2], indices, this./**/model.Xe);
			this.renderBuffer.__numIndices = 0;
		}
	}

	le.prototype = new ke();

	le.prototype./**/fillGeometryBuffers = function (/**/cameraRight, /**/cameraUp, /**/cameraDir) {
		this.renderBuffer.cleanup();
		this.renderBuffer.__lastRenderCall = null;

		this.od.forEach(function (Bd) {
			Bd.ue(/**/cameraRight, /**/cameraUp, /**/cameraDir, this.renderBuffer);
		}, this);

		if (this.renderBuffer.__lastRenderCall)
			this.renderBuffer.pushRenderCall(this.renderBuffer.__lastRenderCall);
	}

	var me = function () {
		this._init = function (we, Ab, Mc, options) {
			me.prototype._init.call(this, we, Ab, Mc, ne, options);

			this.materials = [];
			this./**/model.materials.forEach(function (value) {
				this.materials.push(['source-over', 'lighter', 'multiply'][value]);
			}, this);

			this./**/textureDescs = [];
		}
	}

	me.prototype = new ke();

	me.prototype./**/draw = function (/**/context, /**/camera) {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Hd(/**/context, /**/camera);
		}
	}

	this.createWGLInstance = function (/**/position, /**/rotation, /**/renderBuffer, /**/options) {
		var Ld = new le();
		Ld._init(this, /**/position, /**/rotation, /**/renderBuffer, /**/options || {});
		return Ld;
	}

	this.createCanvas2DInstance = function (/**/position, /**/rotation, /**/options) {
		var Ld = new me();
		Ld._init(this, /**/position, /**/rotation, /**/options || {});
		return Ld;
	}
	this.textures = ['glow_point_red_gold.png','whiteFluff.jpg'];
	this.materials = [1];
	this.renderStyles = [{materialIndex:0,textureIndices:[0]},{materialIndex:0,textureIndices:[1]}];
	this.Xe = 200;

	function Emitter_glimmer() {

		var _2s = [[9.99422, 9.93103], [19.9884, 19.8621], [9.99422, 19.8621], [19.9884, 9.93103], [29.9827, 19.8621], [29.9827, 9.93103], [19.9884, 39.7241], [19.9884, 29.7931], [29.9827, 29.7931], [29.9827, 39.7241], [49.9711, 39.7241], [59.9653, 39.7241], [59.9653, 29.7931], [109.936, 148.966], [49.9711, 69.5172], [109.936, 69.5172], [59.9653, 69.5172]], _1, _2Srch, _2 = [], _3 = [], _5, _7, _9, _10, _11, _12, _12i0, _12s0 = [], _13;
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmer";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._12 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 21;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 21;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2Srch = this._2f(_1 * 585167);
			ctx.yb(_2, -991 + (_2Srch[0] % 173) * 9.99422, -107 + Math.floor(_2Srch[0] / 173) * 9.93103, _2s[_2Srch[1]], Bd.Ld.rand);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			_5 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._6 = _5;
			_7 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._8 = _7;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2Srch = this._2f(_1 * 585167);
			ctx.yb(_2, -991 + (_2Srch[0] % 173) * 9.99422, -107 + Math.floor(_2Srch[0] / 173) * 9.93103, _2s[_2Srch[1]], Bd.Ld.rand);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			_5 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._6 = _5;
			_7 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._8 = _7;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			_9 = Xb._6 + Qb * 33;
			Xb._6 = _9;
			ctx.T(Xb.Ab, Xb._4);
			_10 = 1;
			_11 = (Xb._ / _10);
			_12i0=(_11<0?0:(_11>1?1:_11));
			_12i0<0.204609?ctx.V(_12s0,0,(_12i0-0)*4.88738):ctx.V(_12s0,1,(_12i0-0.204609)*1.25724);
			_12 = Db.nb(Bd._12[0][_12s0[0]],_12s0[1]);
			_13 = (Xb._8 * _12);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._6;
			ctx.V(Xb.Nd,_13,_13);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = 1;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_10 = 1;
			if (Xb._ > _10) return true;
			return false;
		}

			this._2f = function(i) {
				return i<282748?i<59563?i<51705?i<2798?i<1601?i<1?[3135,0]:i<248?i<89?i<2?[3306,0]:[3479,0]:[3652,0]:i<1093?i<585?i<363?[3307,0]:[3308,0]:[3480,3]:[3653,3]:i<1750?i<1730?[3825,0]:[3998,0]:i<2720?i<2258?[3826,3]:i<2466?[3999,0]:[4000,0]:i<2728?[4172,0]:[4173,0]:i<22867?i<11525?i<7279?i<2808?i<2804?i<2802?[3136,3]:[3138,0]:[3139,3]:i<5054?i<4292?i<3798?i<3290?i<3050?[3309,0]:[3310,0]:[3482,3]:i<4038?[3311,0]:[3484,0]:[3655,5]:i<6514?i<6039?i<5529?i<5292?[3312,0]:[3313,0]:[3485,3]:i<6259?[3314,0]:[3487,0]:[3658,5]:i<7852?i<7396?i<7280?[3143,0]:[3144,0]:i<7398?[2972,3]:i<7620?[3145,0]:[3146,0]:i<9995?i<9230?i<8721?i<8211?i<8039?[3315,0]:[3316,0]:[3488,3]:i<8975?[3317,0]:[3490,0]:[3661,5]:[3318,7]:i<15157?i<13319?i<13049?[3828,4]:i<13230?i<13140?[4174,0]:[4175,0]:[4176,0]:i<14849?[3831,4]:i<15033?i<14936?[4177,0]:[4178,0]:[4179,0]:i<19941?i<17901?i<16687?[3834,4]:i<17391?i<17119?i<16865?[4180,0]:[4181,0]:i<17138?[4353,0]:[4354,0]:[4182,2]:[3837,6]:i<21337?i<20950?i<20440?i<20210?i<19958?[4526,0]:[4527,0]:i<20213?[4699,0]:[4700,0]:[4528,2]:i<21082?[4873,0]:[4874,0]:[4529,7]:i<32079?i<27767?i<23213?i<23201?i<23158?i<23057?[3147,0]:[3148,0]:[3149,0]:i<23211?i<23209?[3150,0]:[3151,0]:[3152,0]:i<25507?i<24742?i<24233?[3320,1]:i<24487?[3322,0]:[3495,0]:[3666,5]:i<27002?i<26507?i<25997?i<25756?[3323,0]:[3324,0]:[3496,3]:i<26747?[3325,0]:[3498,0]:[3669,5]:i<28260?i<27968?i<27856?i<27780?[3153,0]:[3154,0]:[3155,0]:i<28084?[3156,0]:[3157,0]:i<30549?i<29784?i<29274?i<28764?i<28509?[3326,0]:[3327,0]:[3499,3]:[3328,2]:[3672,5]:[3329,7]:i<42783?i<38199?[3839,11]:i<40494?[4531,8]:i<42024?[4534,4]:i<42534?[4880,3]:[4882,0]:i<47883?[3845,10]:i<50175?i<49413?[4537,4]:i<49920?i<49665?[4883,0]:[4884,0]:[4885,0]:[4540,7]:i<52421?i<51881?i<51715?[5046,0]:[5047,0]:i<52384?i<52129?[5048,0]:[5049,0]:i<52397?[5221,0]:[5222,0]:i<53840?i<53253?i<52979?i<52931?[5050,3]:i<52956?[5223,0]:[5224,0]:i<53234?[5052,0]:[5225,0]:i<53766?i<53653?i<53649?i<53493?[5053,0]:[5054,0]:[5226,0]:i<53695?[5055,0]:[5228,0]:i<53834?[5401,0]:[5574,0]:i<58850?i<56810?i<55335?i<54825?i<54316?i<54061?[5056,0]:[5057,0]:i<54570?[5229,0]:[5230,0]:[5058,2]:i<56300?i<55844?i<55589?[5402,0]:[5403,0]:i<56045?[5575,0]:[5576,0]:[5404,2]:[5059,6]:i<59139?i<58981?i<58865?[5748,0]:[5749,0]:[5750,0]:i<59515?i<59296?[5751,0]:[5752,0]:[5925,0]:i<158461?i<111956?i<86781?i<67174?i<64083?i<60293?i<60282?i<60108?i<59608?i<59583?[2985,0]:[2986,0]:i<59857?[3158,0]:[3159,0]:[3160,0]:[3161,0]:i<62588?[3331,8]:i<63318?i<63173?i<62742?[3334,0]:i<62994?[3507,0]:[3508,0]:[3509,0]:[3680,5]:i<64108?[3168,0]:i<65697?i<64934?i<64564?i<64130?[3338,0]:i<64318?[3510,0]:[3511,0]:i<64680?[3339,0]:[3512,0]:i<65443?i<65189?[3683,0]:[3684,0]:[3685,0]:i<66666?i<66158?i<65905?[3340,0]:[3341,0]:[3513,3]:[3686,3]:i<77884?[3850,16]:i<82968?i<80936?i<79410?i<78902?i<78393?i<78139?[3856,0]:[3857,0]:i<78648?[4029,0]:[4030,0]:[3858,2]:i<80428?i<79919?i<79665?[4202,0]:[4203,0]:i<80174?[4375,0]:[4376,0]:[4204,2]:[3859,6]:i<85257?i<84494?i<83986?i<83477?i<83223?[4548,0]:[4549,0]:i<83732?[4721,0]:[4722,0]:[4550,2]:i<85003?i<84749?[4894,0]:[4895,0]:[4896,0]:[4551,7]:i<92398?i<90587?i<86869?i<86858?i<86820?[3169,0]:[3170,0]:[3171,0]:i<89139?i<88377?i<87885?[3342,1]:i<88123?[3344,0]:[3517,0]:[3688,5]:i<89825?i<89734?i<89259?i<89251?[3345,0]:[3346,0]:i<89513?[3518,0]:[3519,0]:[3520,0]:[3691,5]:i<91579?i<90817?i<90697?i<90626?[3521,0]:[3522,0]:[3523,0]:[3694,5]:i<91890?i<91728?[3524,0]:[3525,0]:[3697,3]:[3861,15]:i<134727?i<119336?i<115605?i<112003?i<111962?[3184,0]:[3185,0]:i<113462?i<112700?i<112418?i<112193?[3526,0]:[3527,0]:i<112450?[3355,0]:[3528,0]:[3699,5]:i<114843?i<114335?i<113827?i<113597?[3356,0]:[3357,0]:[3529,3]:[3358,2]:[3702,5]:i<115739?i<115725?i<115667?[3186,0]:[3187,0]:[3188,0]:i<118015?i<117253?i<116755?[3359,1]:i<116999?[3361,0]:[3534,0]:[3705,5]:i<118828?i<118320?i<118179?[3362,0]:[3363,0]:[3535,3]:[3708,3]:i<128165?i<125432?[3872,11]:i<127199?i<126904?i<126448?[4564,1]:i<126702?[4566,0]:[4739,0]:i<127135?[4910,0]:[4911,0]:i<127879?i<127707?[4567,3]:i<127829?[4740,0]:[4741,0]:i<128133?[4569,0]:[4742,0]:i<133245?[3878,10]:i<134106?i<133818?i<133753?[4570,3]:i<133786?[4743,0]:[4744,0]:i<134072?[4572,0]:[4745,0]:i<134614?[4573,3]:i<134654?[4746,0]:[4747,0]:i<141280?i<138129?i<136775?i<136013?i<135566?i<135058?i<134875?[3364,0]:[3365,0]:[3537,3]:i<135759?[3366,0]:[3539,0]:[3710,5]:i<137621?i<137113?i<136961?[3367,0]:[3368,0]:[3540,3]:[3713,3]:i<140037?i<139275?i<138908?i<138400?i<138271?[3369,0]:[3370,0]:[3542,3]:i<139021?[3371,0]:[3544,0]:[3715,5]:i<140772?i<140264?i<140149?[3372,0]:[3373,0]:[3545,3]:[3718,3]:i<149571?i<146360?[3883,10]:i<148047?i<147779?i<147271?i<146868?[4575,3]:i<147022?[4748,0]:[4749,0]:[4577,2]:i<147820?[4922,0]:[4923,0]:[4578,7]:[3888,14]:i<245384?i<231038?i<189128?i<180366?i<170198?i<164581?[5061,11]:i<167138?i<166104?i<165594?i<165091?[5753,3]:i<165339?[5926,0]:[5927,0]:[5755,2]:i<166628?i<166473?i<166219?[6099,0]:[6100,0]:i<166474?[6272,0]:[6273,0]:[6101,2]:[5756,9]:i<175282?i<173250?i<171724?i<171216?i<170707?i<170453?[5067,0]:[5068,0]:i<170962?[5240,0]:[5241,0]:[5069,2]:i<172742?i<172233?i<171979?[5413,0]:[5414,0]:i<172488?[5586,0]:[5587,0]:[5415,2]:[5070,6]:i<178334?i<176808?i<176300?i<175791?i<175537?[5759,0]:[5760,0]:i<176046?[5932,0]:[5933,0]:[5761,2]:i<177826?i<177317?i<177063?[6105,0]:[6106,0]:i<177572?[6278,0]:[6279,0]:[6107,2]:[5762,6]:i<182466?i<180550?i<180372?[6446,0]:i<180547?[6447,0]:[6620,0]:i<181968?i<181458?i<181060?[6448,3]:i<181204?[6621,0]:[6622,0]:[6450,2]:i<182125?i<182124?i<181970?[6794,0]:[6795,0]:[6968,0]:i<182379?[6796,0]:[6969,0]:i<187516?i<185484?i<183992?i<183484?i<182975?i<182721?[6451,0]:[6452,0]:i<183230?[6624,0]:[6625,0]:[6453,2]:i<184976?i<184501?i<184247?[6797,0]:[6798,0]:i<184722?[6970,0]:[6971,0]:[6799,2]:[6454,6]:i<187881?i<187630?i<187524?[7143,0]:[7144,0]:i<187855?[7145,0]:[7318,0]:i<188867?i<188389?[7146,3]:i<188613?[7319,0]:[7320,0]:i<188908?[7492,0]:[7493,0]:[5072,13]:i<231052?[7666,0]:i<241612?i<232918?i<232905?i<231692?i<231369?i<231368?i<231162?[7667,0]:[7668,0]:[7841,0]:i<231623?[7669,0]:[7842,0]:i<232889?i<232416?i<232200?[7670,3]:i<232316?[7843,0]:[7844,0]:i<232670?[7672,0]:[7845,0]:[8018,0]:i<232906?[8537,0]:[8710,0]:i<237884?i<235852?i<234442?[7673,4]:i<235344?i<234898?i<234644?[8019,0]:[8020,0]:i<235090?[8192,0]:[8193,0]:[8021,2]:[7676,6]:i<240088?i<239329?i<238821?i<238335?i<238081?[8365,0]:[8366,0]:i<238567?[8538,0]:[8539,0]:[8367,2]:i<239834?i<239580?[8711,0]:[8712,0]:[8713,0]:[8368,7]:i<241627?i<241626?[8883,0]:[9056,0]:i<243680?i<243098?i<242590?i<242131?i<241877?[8884,0]:[8885,0]:i<242336?[9057,0]:[9058,0]:[8886,2]:i<243370?i<243365?i<243147?[9230,0]:[9231,0]:[9404,0]:i<243624?[9232,0]:[9405,0]:i<244696?[8887,1]:i<245204?[9233,3]:i<245293?[9406,0]:[9407,0]:i<272807?i<262564?i<246686?i<245675?i<245581?i<245508?[5083,0]:[5256,0]:i<245629?[5429,0]:[5602,0]:i<245902?i<245752?[5775,0]:[5948,0]:i<246625?i<246156?i<246140?[6121,0]:[6122,0]:i<246410?[6294,0]:[6295,0]:[6296,0]:i<256581?i<252009?i<249722?i<248198?i<247702?[6467,1]:i<247944?[6469,0]:[6642,0]:[6813,4]:i<250485?i<250280?i<249778?i<249763?[6470,0]:[6471,0]:i<250030?[6643,0]:[6644,0]:i<250281?[6472,0]:[6645,0]:[6816,4]:[7159,12]:i<259699?i<258328?i<256804?i<256719?i<256652?[6646,0]:[6647,0]:[6648,0]:[6819,4]:i<258695?i<258334?[6477,0]:i<258471?[6649,0]:[6650,0]:i<259203?[6822,3]:i<259457?[6995,0]:[6996,0]:i<261940?i<261223?[7165,4]:i<261731?[7511,3]:[7513,0]:i<262520?i<262298?i<262194?[7168,0]:[7169,0]:i<262514?[7341,0]:[7342,0]:[7514,0]:i<271230?i<265274?i<264291?i<262804?i<262782?i<262706?[5096,0]:[5269,0]:[5442,0]:i<263804?i<263312?[5097,3]:i<263550?[5270,0]:[5271,0]:i<264186?i<263932?[5443,0]:[5444,0]:i<264189?[5616,0]:[5617,0]:i<265087?i<264483?i<264332?[5788,0]:[5961,0]:i<264603?i<264490?[6133,0]:[6306,0]:i<264833?[6134,0]:[6307,0]:i<265100?[5962,0]:i<265179?[6135,0]:[6308,0]:i<270189?i<268157?i<266798?[5099,4]:i<267649?i<267306?[5445,3]:i<267466?[5618,0]:[5619,0]:[5447,2]:[5102,6]:i<270441?i<270204?[5792,0]:i<270420?[5793,0]:[5966,0]:i<270949?[5794,3]:i<271072?[5967,0]:[5968,0]:i<272781?i<272455?i<272102?i<271595?i<271345?[6478,0]:[6479,0]:i<271849?[6651,0]:[6652,0]:i<272345?[6480,0]:[6653,0]:i<272745?i<272664?[6824,0]:[6825,0]:[6997,0]:[6481,0]:i<280417?i<279701?i<277387?i<275849?i<274331?[7678,4]:i<275347?[8024,1]:i<275600?[8026,0]:[8199,0]:i<277333?i<276830?i<276357?[7681,3]:i<276602?[7854,0]:[7855,0]:i<277084?[7683,0]:[7856,0]:i<277376?i<277362?[8027,0]:[8200,0]:[8029,0]:i<279590?i<278828?i<278403?[8370,1]:i<278611?[8372,0]:[8545,0]:[8716,5]:i<279602?[8546,0]:[8719,0]:i<280416?i<280415?i<280158?i<279955?[7684,0]:[7685,0]:i<280369?[7857,0]:[7858,0]:[7686,0]:[8030,0]:i<282586?i<281933?i<281433?[8889,1]:i<281687?[8891,0]:[9064,0]:i<282512?i<282419?i<282187?[9235,0]:[9236,0]:i<282497?[9408,0]:[9409,0]:[9237,0]:i<282705?[8892,0]:[9065,0]:i<395024?i<375747?i<307518?i<306193?i<288605?i<287088?i<282829?i<282802?i<282765?[3204,0]:[3205,0]:[3206,0]:i<284806?i<284044?i<283594?i<283086?i<282949?[3374,0]:[3375,0]:[3547,3]:i<283790?[3376,0]:[3549,0]:[3720,5]:i<286326?i<285819?i<285311?i<285057?[3377,0]:[3378,0]:[3550,3]:i<286072?[3379,0]:[3552,0]:[3723,5]:i<287089?[3207,0]:i<288360?i<287709?i<287302?i<287281?[3380,0]:[3381,0]:i<287556?[3553,0]:[3554,0]:i<288199?i<287963?[3726,0]:[3727,0]:[3728,0]:i<288516?[3729,0]:[3730,0]:i<298566?i<294701?[3893,11]:i<296982?i<296225?[4585,4]:i<296733?[4931,3]:[4933,0]:i<298503?i<297995?i<297490?[4588,3]:i<297741?[4761,0]:[4762,0]:[4590,2]:i<298546?i<298533?[4934,0]:[4935,0]:[4936,0]:i<303646?[3899,10]:i<305233?i<305170?[4591,4]:i<305213?i<305191?[4937,0]:[4938,0]:[4939,0]:i<306179?i<305741?[4594,3]:i<305991?[4767,0]:[4768,0]:[4940,0]:i<306195?[3731,0]:i<307295?i<306615?i<306329?[3904,0]:i<306579?[4077,0]:[4078,0]:i<306969?i<306869?[4250,0]:[4251,0]:i<307223?[4423,0]:[4424,0]:i<307493?i<307490?[4596,0]:[4597,0]:[4769,0]:i<351210?i<327807?i<316263?i<308019?i<307529?[633,0]:i<307718?i<307605?[806,0]:[979,0]:i<307851?[1152,0]:[1325,0]:i<312398?i<310513?i<309046?i<308565?i<308194?i<308029?[115,0]:[116,0]:i<308310?[288,0]:[289,0]:i<308791?[117,0]:[290,0]:i<310003?i<309500?i<309245?[461,0]:[462,0]:i<309748?[634,0]:[635,0]:[463,2]:i<311388?i<310878?i<310708?[118,0]:[119,0]:[291,3]:i<311898?[464,3]:i<312152?[637,0]:[638,0]:i<315458?[807,9]:i<315811?i<315671?i<315668?[810,0]:[811,0]:[983,0]:i<315928?[1156,0]:i<316111?[1329,0]:[1330,0]:i<319480?i<317806?i<316743?i<316289?[1670,0]:i<316491?[1498,0]:[1671,0]:i<317296?i<316893?i<316750?[1842,0]:[1843,0]:i<317041?[2015,0]:[2016,0]:[1844,2]:i<319162?i<318652?i<318271?i<318016?[2188,0]:[2189,0]:i<318398?[2361,0]:[2362,0]:[2190,2]:i<319255?i<319163?[2534,0]:[2535,0]:[2536,0]:i<323985?i<322426?i<321010?[1499,4]:i<322030?[1845,1]:i<322282?[1847,0]:[2020,0]:i<323446?[1502,1]:i<323947?i<323696?[1848,0]:[1849,0]:i<323965?[2021,0]:[2022,0]:i<326280?[2191,8]:i<327297?i<326787?i<326535?[2194,0]:[2195,0]:[2367,3]:[2540,3]:i<340311?i<333885?i<331659?i<330339?i<328809?i<328543?i<328033?i<327953?[120,0]:[121,0]:[293,3]:i<328556?[122,0]:[295,0]:[466,4]:i<330640?i<330546?[296,0]:[297,0]:i<331149?i<330895?[469,0]:[470,0]:[642,3]:i<332450?i<331998?i<331819?i<331702?[812,0]:[813,0]:[814,0]:i<332301?i<332150?[1331,0]:[1332,0]:[1333,0]:i<333242?i<332956?i<332701?[815,0]:[816,0]:i<333012?[988,0]:[989,0]:i<333442?[1162,0]:i<333632?[1334,0]:[1335,0]:i<335681?i<334731?i<333890?[298,0]:i<334669?i<334166?i<334099?[471,0]:[472,0]:i<334421?[644,0]:[645,0]:[646,0]:i<334829?[302,0]:i<335194?i<334940?[474,0]:[475,0]:i<335426?[647,0]:[648,0]:i<338522?i<337084?i<336701?[817,1]:i<336877?[819,0]:[992,0]:i<338104?[1163,1]:i<338314?[1165,0]:[1338,0]:i<339470?i<339015?i<338760?[820,0]:[821,0]:i<339215?[993,0]:[994,0]:i<339900?i<339645?[1166,0]:[1167,0]:i<340056?[1339,0]:[1340,0]:i<348814?i<345156?i<343151?i<341841?[1504,4]:i<342641?i<342348?i<342093?[1850,0]:[1851,0]:i<342387?[2023,0]:[2024,0]:[1852,2]:i<344171?[1507,1]:i<344665?i<344426?[1853,0]:[1854,0]:i<344920?[2026,0]:[2027,0]:i<347440?i<346675?i<346165?i<345655?i<345400?[2196,0]:[2197,0]:[2369,3]:[2198,2]:[2542,5]:i<348450?i<347950?[2199,3]:i<348205?[2372,0]:[2373,0]:i<348703?[2545,0]:[2546,0]:i<351096?i<350052?i<349937?i<349772?i<349324?[1509,3]:i<349579?[1682,0]:[1683,0]:i<349923?[1511,0]:[1684,0]:i<350039?i<350036?[1855,0]:[1856,0]:[2028,0]:i<350743?i<350435?i<350180?[1512,0]:[1513,0]:i<350488?[1685,0]:[1686,0]:i<350966?i<350744?[1858,0]:[1859,0]:[2032,0]:i<351152?i<351139?[2201,0]:[2374,0]:i<351207?[2205,0]:[2378,0]:i<365472?i<358338?i<352646?i<351224?i<351217?[2709,0]:[3228,0]:i<351226?[3744,0]:i<351952?i<351499?i<351250?[3400,0]:i<351279?[3572,0]:[3573,0]:i<351697?[3401,0]:[3574,0]:i<352391?i<352136?[3745,0]:[3746,0]:[3747,0]:i<354961?i<353797?i<353144?i<352909?i<352732?[2710,0]:[2711,0]:[2712,0]:i<353501?i<353147?[3057,0]:i<353281?[3229,0]:[3230,0]:i<353543?[3058,0]:[3231,0]:i<354278?i<354277?i<354036?[2713,0]:[2714,0]:[2887,0]:i<354451?i<354354?[3059,0]:[3060,0]:[3232,3]:i<357160?i<356491?[3402,4]:i<356996?i<356746?[3748,0]:[3749,0]:[3750,0]:i<358180?[3405,1]:i<358269?[3751,0]:[3752,0]:i<364751?i<362011?i<359119?i<358623?i<358425?[3917,0]:[4090,0]:i<358630?i<358626?[4262,0]:[4435,0]:i<358873?[4263,0]:[4436,0]:i<360647?i<360139?[3918,1]:i<360394?[3920,0]:[4093,0]:i<361667?[4264,1]:i<361851?[4266,0]:[4439,0]:i<362521?i<362420?i<362240?[4609,0]:[4782,0]:[4955,0]:i<363986?i<363541?[4610,1]:i<363737?[4612,0]:[4785,0]:[4956,5]:i<365136?i<365055?i<364989?[3921,0]:[3922,0]:[4094,0]:i<365380?i<365157?[4786,0]:i<365345?i<365306?[4959,0]:[4960,0]:[4961,0]:i<365418?[4962,0]:[4963,0]:i<371304?i<370913?i<367475?i<367209?i<366228?i<365996?i<365978?i<365724?[2715,0]:[2716,0]:[2888,3]:i<366225?[2717,0]:[2890,0]:i<366940?i<366430?i<366336?[3061,0]:[3062,0]:[3234,3]:i<366959?[3063,0]:[3236,0]:i<367302?[2718,0]:i<367462?[3237,0]:[3238,0]:i<369423?i<369005?[3407,4]:i<369180?i<369057?[3753,0]:[3754,0]:[3755,0]:i<370403?i<369893?i<369678?[3410,0]:[3411,0]:[3583,3]:[3756,3]:i<371080?i<370933?[3412,0]:[3585,0]:[3758,0]:i<372261?i<372063?i<371406?[3928,0]:i<371915?i<371660?[3929,0]:[3930,0]:i<371980?[4102,0]:[4103,0]:i<372260?i<372223?i<372135?[4964,0]:[4965,0]:[4966,0]:[4968,0]:i<373562?i<372596?i<372427?i<372421?[3931,0]:[4104,0]:i<372432?[4279,0]:[4452,0]:i<372656?[4108,0]:i<373053?i<372801?[4280,0]:[4281,0]:i<373308?[4453,0]:[4454,0]:i<375176?i<374438?i<373929?i<373610?[4624,0]:i<373696?[4796,0]:[4797,0]:i<374183?[4625,0]:[4798,0]:i<374922?i<374667?[4969,0]:[4970,0]:[4971,0]:i<375695?i<375541?i<375428?[4626,0]:[4627,0]:[4799,0]:[4972,0]:i<380927?i<379753?i<378795?[5104,9]:i<379189?i<378937?[5107,0]:i<379176?[5280,0]:[5281,0]:i<379493?i<379443?[5453,0]:[5454,0]:i<379735?[5626,0]:[5627,0]:i<380834?i<380552?i<380261?[5796,3]:i<380421?[5969,0]:[5970,0]:i<380798?[5798,0]:[5971,0]:[5799,0]:i<387145?i<382749?i<380947?[5128,0]:i<382346?i<381836?i<381446?i<381191?[5129,0]:[5130,0]:i<381581?[5302,0]:[5303,0]:[5131,2]:i<382482?i<382350?[5475,0]:[5476,0]:i<382727?[5477,0]:[5650,0]:i<385383?i<384279?[5132,4]:i<385011?i<384789?[5478,3]:i<384892?[5651,0]:[5652,0]:i<385266?[5480,0]:[5653,0]:i<386403?[5135,1]:i<386913?[5481,3]:i<387033?[5654,0]:[5655,0]:i<389828?i<389603?i<388668?i<388165?[5137,1]:i<388413?[5139,0]:[5312,0]:i<389339?i<389178?[5483,3]:i<389269?[5656,0]:[5657,0]:i<389580?[5485,0]:[5658,0]:i<389782?i<389659?i<389657?[5140,0]:[5141,0]:[5313,0]:[5486,0]:i<392930?i<392227?i<391251?i<390745?i<390323?i<390068?[5142,0]:[5143,0]:i<390490?[5315,0]:[5316,0]:i<390997?[5144,0]:[5317,0]:i<391717?i<391548?i<391298?[5488,0]:[5489,0]:[5662,0]:[5490,2]:i<392312?i<392247?[5145,0]:[5318,0]:i<392504?i<392500?[5491,0]:[5492,0]:i<392758?[5664,0]:[5665,0]:i<393414?i<393388?i<392976?[5835,0]:i<393227?[5836,0]:[6009,0]:[6182,0]:i<394434?[5837,1]:i<394905?i<394650?[6183,0]:[6184,0]:i<394918?[6356,0]:[6357,0]:i<520551?i<479312?i<441061?i<420210?i<406599?i<402572?i<396862?i<396842?i<395386?i<395357?i<395196?[303,0]:[304,0]:[305,0]:i<396406?[476,1]:i<396590?[478,0]:[651,0]:[652,0]:i<399836?i<398392?[822,4]:i<399411?i<398902?[1168,3]:i<399157?[1341,0]:[1342,0]:i<399666?[1170,0]:[1343,0]:i<401081?i<400728?i<400218?i<400058?[825,0]:[826,0]:[998,3]:i<400826?[827,0]:[1000,0]:i<402062?i<401591?[1171,3]:i<401810?[1344,0]:[1345,0]:[1173,2]:i<402577?[659,0]:i<404688?i<403158?i<403037?i<402596?[828,0]:i<402845?[1001,0]:[1002,0]:[1003,0]:[1174,4]:i<405579?i<405071?i<404827?[831,0]:[832,0]:i<405324?[1004,0]:[1005,0]:[1177,1]:i<414360?i<409940?i<409348?i<407833?i<407619?[1514,1]:i<407683?[1516,0]:[1689,0]:i<408853?[1860,1]:i<409093?[1862,0]:[2035,0]:i<409472?i<409369?[1518,0]:[1519,0]:i<409584?i<409482?[1863,0]:i<409542?[2036,0]:[2037,0]:i<409695?[1865,0]:[2038,0]:i<412162?i<411452?i<410942?i<410450?[2206,3]:i<410687?[2379,0]:[2380,0]:[2208,2]:i<411907?i<411652?[2552,0]:[2553,0]:[2554,0]:i<413595?i<413085?i<412575?i<412339?[2209,0]:[2210,0]:[2382,3]:[2211,2]:[2555,5]:i<418595?i<417246?i<415718?i<415208?i<414804?i<414549?[1520,0]:[1521,0]:i<414953?[1693,0]:[1694,0]:[1522,2]:i<416737?i<416227?i<415972?[1866,0]:[1867,0]:[2039,3]:i<416992?[1868,0]:[2041,0]:i<418209?i<417756?[1523,3]:i<418011?[1696,0]:[1697,0]:i<418485?i<418453?[1869,0]:[1870,0]:[2042,0]:i<420003?i<419730?i<419568?i<419105?[2212,3]:i<419360?[2385,0]:[2386,0]:i<419722?[2214,0]:[2387,0]:i<419962?[2558,0]:[2559,0]:i<420040?[2389,0]:[2562,0]:i<428054?i<423997?i<422363?i<420833?i<420781?i<420410?i<420398?[833,0]:[834,0]:i<420665?[1006,0]:[1007,0]:[1008,0]:[1179,4]:i<422467?i<422440?i<422413?[1009,0]:[1010,0]:[1011,0]:[1182,4]:i<426485?i<424955?i<424572?i<424128?i<424014?[839,0]:[840,0]:i<424317?[1012,0]:[1013,0]:i<424700?[841,0]:[1014,0]:[1185,4]:i<427034?i<426580?i<426573?[842,0]:[843,0]:i<426835?[1015,0]:[1016,0]:[1188,1]:i<434196?i<430246?i<429105?i<428744?i<428563?i<428308?[1525,0]:[1526,0]:i<428622?[1698,0]:[1699,0]:i<428999?[1527,0]:[1700,0]:i<429861?i<429615?[1528,3]:i<429732?[1701,0]:[1702,0]:i<430116?[1530,0]:[1703,0]:i<432009?i<431244?i<430818?i<430346?i<430252?[2217,0]:[2218,0]:i<430563?[2390,0]:[2391,0]:i<430989?[2219,0]:[2392,0]:[2563,5]:i<433431?i<432956?i<432446?i<432226?[2220,0]:[2221,0]:[2393,3]:i<433176?[2222,0]:[2395,0]:[2566,5]:i<437327?i<435540?i<435504?i<435009?i<434706?[1531,3]:i<434838?[1704,0]:[1705,0]:i<435264?[1533,0]:[1706,0]:[1879,2]:i<436560?[1534,1]:i<436953?i<436698?[1880,0]:[1881,0]:i<437072?[2053,0]:[2054,0]:i<439541?i<438776?i<438275?i<437765?i<437545?[2223,0]:[2224,0]:[2396,3]:i<438521?[2225,0]:[2398,0]:[2569,5]:i<440561?[2226,1]:i<440816?[2572,0]:[2573,0]:i<457358?i<445989?i<445254?i<444961?i<443298?i<442459?i<441949?i<441513?i<441258?[2725,0]:[2726,0]:i<441694?[2898,0]:[2899,0]:[2727,2]:i<442906?i<442810?i<442556?[3071,0]:[3072,0]:i<442811?[3244,0]:[3245,0]:i<443161?[3073,0]:[3246,0]:i<444586?i<444296?i<443808?[2728,3]:i<444063?[2901,0]:[2902,0]:i<444536?[2730,0]:[2903,0]:i<444905?i<444836?[3074,0]:[3075,0]:[3247,0]:i<445000?[3765,0]:i<445196?i<445096?[3766,0]:[3767,0]:[3768,0]:i<445963?i<445311?[2731,0]:i<445695?i<445504?[2735,0]:[2908,0]:i<445850?[3081,0]:[3254,0]:[3427,0]:i<451709?i<451594?i<448536?i<447162?i<446655?i<446176?i<446010?[3936,0]:[3937,0]:i<446400?[4109,0]:[4110,0]:i<446907?[3938,0]:[4111,0]:i<448052?i<447672?[4282,3]:i<447879?[4455,0]:[4456,0]:i<448307?[4284,0]:[4457,0]:i<450064?i<449556?[3939,1]:i<449809?[3941,0]:[4114,0]:[4285,4]:i<451600?i<451596?[4628,0]:[4630,0]:i<451667?i<451632?[4631,0]:[4632,0]:[4633,0]:i<454611?i<454163?i<452633?i<452472?i<451963?i<451895?[3942,0]:[3943,0]:i<452218?[4115,0]:[4116,0]:[4117,0]:[4288,4]:i<454171?[4118,0]:i<454314?[4291,0]:i<454563?[4464,0]:[4465,0]:i<455878?i<455595?i<455085?i<454968?i<454723?[4634,0]:[4635,0]:[4808,0]:[4636,2]:i<455625?[4981,0]:[4982,0]:i<456848?i<456338?i<456133?[4637,0]:[4638,0]:[4810,3]:[4983,3]:i<470715?i<465473?i<461467?i<460384?i<458857?i<458378?[2736,1]:i<458631?[2738,0]:[2911,0]:i<459877?[3082,1]:i<460129?[3084,0]:[3257,0]:i<460974?i<460782?i<460589?[2739,0]:[2740,0]:[2741,0]:i<461356?i<460995?[3085,0]:i<461221?[3258,0]:[3259,0]:[3260,0]:i<463178?i<462788?i<462278?i<461965?i<461710?[3428,0]:[3429,0]:i<462037?[3601,0]:[3602,0]:[3430,2]:i<462923?[3775,0]:[3776,0]:[3431,8]:i<467322?i<466574?i<466043?i<465856?i<465664?[2742,0]:[2743,0]:[2744,0]:i<466291?i<466150?[3261,0]:[3262,0]:i<466329?[3090,0]:[3263,0]:i<466712?i<466689?[2745,0]:[2746,0]:i<466838?i<466811?[3091,0]:[3092,0]:i<467093?[3264,0]:[3265,0]:i<469596?i<468852?[3434,4]:i<469362?[3780,3]:[3782,0]:i<470561?i<470105?i<469851?[3437,0]:[3438,0]:i<470360?[3610,0]:[3611,0]:i<470699?[3783,0]:[3784,0]:i<475199?i<473291?i<471171?i<470813?i<470811?[3948,0]:[4121,0]:i<471067?[3949,0]:[4122,0]:i<472643?i<472133?i<471681?[3950,3]:i<471885?[4123,0]:[4124,0]:[3952,2]:i<472782?i<472687?[4297,0]:[4470,0]:i<473036?[4298,0]:[4471,0]:i<473729?i<473424?i<473295?[4639,0]:[4812,0]:i<473676?[4985,0]:[4986,0]:i<474635?i<474125?i<473892?[4643,0]:i<473895?[4815,0]:[4816,0]:[4644,2]:i<474944?i<474689?[4988,0]:[4989,0]:[4990,0]:i<477130?i<477127?i<475935?i<475920?i<475487?i<475443?[3953,0]:[3954,0]:i<475739?[4126,0]:[4127,0]:i<475924?[3955,0]:[4128,0]:i<476955?[4299,1]:i<477048?[4301,0]:[4474,0]:[4476,0]:i<478544?i<478149?i<478108?i<477639?i<477385?[4645,0]:[4646,0]:i<477894?[4818,0]:[4819,0]:[4647,0]:i<478404?[4991,0]:[4992,0]:i<478933?i<478670?[4649,0]:i<478689?[4821,0]:[4822,0]:i<479057?[4994,0]:[4995,0]:i<493294?i<489025?i<479586?i<479324?[1017,0]:i<479419?[1190,0]:i<479582?[1363,0]:[1364,0]:i<484615?i<482318?i<481827?i<480587?i<480513?i<480004?i<479841?[1536,0]:[1537,0]:i<480259?[1709,0]:[1710,0]:i<480514?[1538,0]:[1711,0]:i<481607?[1882,1]:[1884,2]:i<481932?i<481828?[1541,0]:[1714,0]:i<482105?[1887,0]:[2060,0]:i<483612?i<483417?i<483312?i<482828?[2228,3]:i<483083?[2401,0]:[2402,0]:i<483404?[2230,0]:[2403,0]:i<483576?[2574,0]:[2575,0]:i<484148?i<483655?[2405,0]:i<483893?[2233,0]:[2406,0]:i<484360?i<484170?[2577,0]:[2578,0]:[2579,0]:i<486973?i<485501?i<485311?i<484802?i<484697?[1542,0]:[1543,0]:i<485056?[1715,0]:[1716,0]:i<485324?[1544,0]:[1717,0]:i<486521?[1888,1]:i<486759?[1890,0]:[2063,0]:i<488981?i<488326?i<487993?[2234,1]:i<488172?[2236,0]:[2409,0]:i<488836?[2580,3]:[2582,0]:i<489019?[2583,0]:[2584,0]:i<492330?i<490001?i<489123?i<489031?[1895,0]:[2068,0]:i<489491?i<489279?[1896,0]:[1897,0]:[2069,3]:i<490800?i<490452?i<490020?[2413,0]:i<490200?[2241,0]:[2414,0]:i<490545?[2586,0]:[2587,0]:[2242,7]:i<492699?i<492431?[1898,0]:i<492681?[2071,0]:[2072,0]:i<493197?i<492986?i<492953?[2244,0]:[2245,0]:i<493195?[2417,0]:[2418,0]:[2590,0]:i<509758?i<503202?i<497653?i<495693?i<493307?[3266,0]:i<494605?i<494095?i<493704?i<493449?[2750,0]:[2751,0]:i<493840?[2923,0]:[2924,0]:[2752,2]:i<495183?i<494869?i<494621?[3096,0]:[3097,0]:i<494928?[3269,0]:[3270,0]:[3098,2]:i<495746?i<495742?[3439,0]:[3612,0]:i<497004?i<496494?i<496111?i<495856?[3442,0]:[3443,0]:i<496239?[3615,0]:[3616,0]:[3444,2]:i<497398?i<497143?[3788,0]:[3789,0]:[3790,0]:i<502332?i<500462?i<499183?[2753,4]:i<500117?i<499693?[3099,3]:i<499948?[3272,0]:[3273,0]:i<500372?[3101,0]:[3274,0]:i<501472?i<500962?i<500716?[2756,0]:[2757,0]:[2929,3]:i<501982?[3102,3]:i<502126?[3275,0]:[3276,0]:i<502939?i<502654?i<502587?[3445,0]:[3446,0]:i<502908?[3618,0]:[3619,0]:i<503192?[3791,0]:[3792,0]:i<507763?i<505427?i<504112?i<503792?i<503330?i<503222?[4304,0]:[4305,0]:i<503537?[4477,0]:[4478,0]:i<503859?[4306,0]:[4479,0]:i<505296?i<504787?i<504502?i<504247?[3961,0]:[3962,0]:i<504548?[4134,0]:[4135,0]:i<505042?[3963,0]:[4136,0]:i<505387?i<505313?[4308,0]:[4480,0]:[4309,0]:i<507594?i<506953?i<506447?[4650,1]:i<506702?[4652,0]:[4825,0]:i<507463?[4996,3]:[4998,0]:i<507719?[4653,0]:[4826,0]:i<508211?i<508176?i<508175?i<508021?i<508013?[3964,0]:[3965,0]:[4137,0]:[4310,0]:i<508200?[4141,0]:[4314,0]:i<508719?i<508303?[4831,0]:i<508465?i<508310?[5002,0]:[5003,0]:[5004,0]:i<509248?i<508756?i<508732?[4659,0]:[4660,0]:i<508996?[4832,0]:[4833,0]:[5005,3]:i<517206?i<517181?i<514389?i<512690?i<511172?i<510662?i<510152?i<509937?[2758,0]:[2759,0]:[2931,3]:[2760,2]:i<512180?i<511682?[3104,3]:i<511925?[3277,0]:[3278,0]:[3106,2]:i<513649?i<513197?i<512945?[2761,0]:[2762,0]:i<513452?[2934,0]:[2935,0]:i<514024?i<513904?[3107,0]:[3108,0]:i<514279?[3280,0]:[3281,0]:i<516309?i<515630?i<515120?i<514732?i<514477?[3450,0]:[3451,0]:i<514865?[3623,0]:[3624,0]:[3452,2]:i<516054?i<515799?[3796,0]:[3797,0]:[3798,0]:i<516927?i<516644?i<516564?[3453,0]:[3454,0]:i<516898?[3626,0]:[3627,0]:i<517175?[3799,0]:[3800,0]:[2763,0]:i<520193?i<519720?i<518684?i<518174?i<517666?i<517411?[3969,0]:[3970,0]:i<517919?[4142,0]:[4143,0]:[3971,2]:i<519375?i<519176?i<518921?[4315,0]:[4316,0]:i<519227?[4488,0]:[4489,0]:i<519629?[4317,0]:[4490,0]:i<520116?i<519936?[3972,0]:[4145,0]:[4318,0]:i<520312?[4834,0]:i<520550?[5007,0]:[5008,0]:i<562046?i<560120?i<539400?i<532795?i<526012?i<520640?i<520584?i<520577?[5666,0]:[5667,0]:[5668,0]:i<523597?i<522151?i<521643?i<521133?i<520890?[5839,0]:[5840,0]:[6012,3]:i<521896?[5841,0]:[6014,0]:i<523087?i<522661?[6185,3]:i<522833?[6358,0]:[6359,0]:[6187,2]:i<524482?i<524221?i<523723?i<523713?[5842,0]:[5843,0]:i<523977?[6015,0]:[6016,0]:i<524236?[5844,0]:[6017,0]:[6188,4]:i<529319?i<527280?i<526522?i<526027?i<526016?[5154,0]:[5327,0]:i<526273?[5155,0]:[5328,0]:i<526770?i<526585?[5500,0]:i<526586?[5672,0]:[5673,0]:[5501,2]:i<528300?[5156,1]:i<528809?i<528554?[5502,0]:[5503,0]:[5675,3]:i<531853?i<530738?i<530228?i<529718?i<529464?[5845,0]:[5846,0]:[6018,3]:[5847,2]:i<531716?i<531247?i<530993?[6191,0]:[6192,0]:i<531502?[6364,0]:[6365,0]:i<531852?[6193,0]:[6366,0]:i<532772?i<532363?[5848,3]:i<532604?[6021,0]:[6022,0]:[6194,0]:i<538591?i<537723?i<534839?i<533847?i<533337?i<533064?i<532810?[6531,0]:[6532,0]:i<533082?[6704,0]:[6705,0]:[6533,2]:i<534329?i<534114?i<533860?[6877,0]:[6878,0]:[7051,0]:[6879,2]:i<536369?[6534,4]:i<537388?i<536879?[6880,3]:i<537134?[7053,0]:[7054,0]:i<537632?[6882,0]:[7055,0]:i<538113?i<537812?[7224,0]:i<538065?[7225,0]:[7398,0]:i<538523?i<538367?[7226,0]:[7227,0]:i<538589?[7399,0]:[7400,0]:i<539075?i<539038?i<538885?i<538832?[6537,0]:[6538,0]:[6710,0]:[6883,0]:i<539278?i<539122?[7233,0]:[7406,0]:[7579,0]:i<554312?i<548620?i<544460?i<541584?i<540403?i<540366?i<539857?i<539655?[5158,0]:[5159,0]:i<540112?[5331,0]:[5332,0]:[5333,0]:i<541422?i<540913?[5504,3]:i<541168?[5677,0]:[5678,0]:i<541495?[5506,0]:[5679,0]:i<542935?i<542425?i<541970?i<541715?[5161,0]:[5162,0]:i<542170?[5334,0]:[5335,0]:[5163,2]:i<543950?i<543440?i<543185?[5507,0]:[5508,0]:[5680,3]:[5509,2]:i<545739?i<545255?i<544948?i<544865?i<544714?[5850,0]:[5851,0]:i<544947?[6023,0]:[6024,0]:i<545060?[5852,0]:[6025,0]:i<545257?i<545256?[6197,0]:[6370,0]:[6198,2]:i<547260?i<546759?[5853,1]:i<547014?[5855,0]:[6028,0]:i<548280?[6199,1]:i<548462?[6201,0]:[6374,0]:i<551601?i<549614?i<549182?i<549172?i<548933?i<548875?[5164,0]:[5165,0]:i<549168?[5337,0]:[5338,0]:[5339,0]:i<549475?i<549357?[5510,0]:[5683,0]:i<549511?[5512,0]:[5685,0]:i<550581?i<550075?i<549820?[5167,0]:[5168,0]:i<550326?[5340,0]:[5341,0]:[5513,1]:i<552272?i<551974?i<551701?i<551686?[5856,0]:[6029,0]:i<551831?[5858,0]:[6031,0]:i<552117?[6204,0]:[6377,0]:[5859,6]:i<558619?i<555556?i<554799?i<554491?i<554483?[6544,0]:[6717,0]:i<554663?i<554517?[7061,0]:[7062,0]:[7063,0]:i<555543?i<555455?i<555307?i<555054?[6545,0]:[6546,0]:i<555394?[6718,0]:[6719,0]:[6547,0]:[7064,0]:i<557827?i<557062?i<556552?i<556042?i<555787?[7234,0]:[7235,0]:[7407,3]:[7236,2]:[7580,5]:i<558284?i<557994?[7237,0]:i<558247?[7410,0]:[7411,0]:i<558539?[7583,0]:[7584,0]:i<558897?i<558895?i<558785?[6550,0]:[6723,0]:[6896,0]:i<559916?i<559407?[6551,3]:i<559661?[6724,0]:[6725,0]:i<560007?[6897,0]:[6898,0]:i<560137?[7752,0]:i<561559?i<561479?i<560970?i<560621?i<560366?[7753,0]:[7754,0]:i<560718?[7926,0]:[7927,0]:i<561225?[7755,0]:[7928,0]:i<561517?[8100,0]:[8101,0]:i<561879?i<561813?[7756,0]:[7757,0]:i<562043?[7929,0]:[7930,0]:i<584461?i<579292?i<570292?i<565770?i<564038?i<562983?i<562968?i<562542?i<562301?[5169,0]:[5170,0]:i<562797?[5342,0]:[5343,0]:[5171,0]:i<563785?i<563368?i<563238?[5515,0]:[5516,0]:i<563623?[5688,0]:[5689,0]:i<563806?[5517,0]:[5690,0]:i<564347?i<564100?[5346,0]:i<564124?[5174,0]:[5347,0]:i<565260?i<564750?i<564499?[5518,0]:[5519,0]:[5691,3]:[5520,2]:i<568730?i<567300?[5861,4]:i<568319?i<567810?[6207,3]:i<568065?[6380,0]:[6381,0]:i<568574?[6209,0]:[6382,0]:i<570094?i<569728?i<569240?[5864,3]:i<569495?[6037,0]:[6038,0]:i<569983?[5866,0]:[6039,0]:i<570290?i<570267?[6210,0]:[6211,0]:[6383,0]:i<575332?i<573293?i<571763?i<571253?i<570743?i<570488?[5175,0]:[5176,0]:[5348,3]:[5177,2]:[5521,4]:i<574313?[5178,1]:i<574823?[5524,3]:i<575078?[5697,0]:[5698,0]:i<577707?i<576543?i<576033?i<575760?i<575518?[5867,0]:[5868,0]:i<575779?[6040,0]:[6041,0]:[5869,2]:i<577197?i<576844?i<576589?[6213,0]:[6214,0]:i<576942?[6386,0]:[6387,0]:[6215,2]:i<578581?i<578173?i<577962?[5870,0]:[5871,0]:i<578428?[6043,0]:[6044,0]:i<578954?i<578836?[6216,0]:[6217,0]:i<579209?[6389,0]:[6390,0]:i<580176?i<580138?i<579924?i<579909?i<579690?i<579547?[6553,0]:[6554,0]:i<579896?[6726,0]:[6727,0]:[6899,0]:i<579926?[6731,0]:i<580008?[6904,0]:[7077,0]:[7250,0]:i<583812?i<583146?i<581618?i<581108?i<580610?i<580355?[6559,0]:[6560,0]:i<580853?[6732,0]:[6733,0]:[6561,2]:i<582638?[6905,1]:i<582893?[6907,0]:[7080,0]:i<583622?i<583421?i<583399?[6562,0]:[6563,0]:[6735,0]:i<583763?[6908,0]:[7081,0]:i<584323?i<584294?i<584041?[7251,0]:[7252,0]:i<584301?[7424,0]:[7425,0]:[7253,0]:i<585165?i<584957?i<584725?i<584715?[5180,0]:[5181,0]:i<584955?[5353,0]:[5354,0]:i<585107?[5526,0]:[5699,0]:[5872,0];
			}

	}

	function Emitter_bgFluff() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _3i, _3s = [], _4, _5 = [], _6 = [], _8 = [], _11, _13=[], _14, _15, _16, _16i0, _16s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:1}];
		this.name = "bgFluff";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-977.6,-457.798],[-976.356,451.578],[-976.356,451.578]]
			];
			Bd._3 = [
				[[982.577,457.074],[979.674,-454.171],[979.674,-454.171]]
			];
			Bd._16 = [
				[
					[0,0.127869,0.127869],
					[0.127869,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 2;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 2;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			_3i = Db.kb(_1);
			ctx.V(_3s,0,(_3i-0)*1);
			Db.lb(_3, Bd._3[_3s[0]], _3s[1]);
			_4 = 0 + Bd.Ld.rand() * (1 - 0);
			ctx.Y(_5, _2, _3, _4);
			ctx.W(_6, _5[0], _5[1], 0);
			Xb._7 = [];
			ctx.c(Xb._7, Bd.Ab, _6);
			ctx.randv3gen(_8, 100, Bd.Ld.rand);
			Xb._9 = [];
			ctx.T(Xb._9, _8);
			Xb._10 = 0;
			_11 = 454 + Bd.Ld.rand() * (989 - 454);
			Xb._12 = _11;
			ctx.T(Xb.Ab, Xb._7);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			_3i = Db.kb(_1);
			ctx.V(_3s,0,(_3i-0)*1);
			Db.lb(_3, Bd._3[_3s[0]], _3s[1]);
			_4 = 0 + Bd.Ld.rand() * (1 - 0);
			ctx.Y(_5, _2, _3, _4);
			ctx.W(_6, _5[0], _5[1], 0);
			Xb._7 = [];
			ctx.c(Xb._7, Bd.Ab, _6);
			ctx.randv3gen(_8, 100, Bd.Ld.rand);
			Xb._9 = [];
			ctx.T(Xb._9, _8);
			Xb._10 = 0;
			_11 = 454 + Bd.Ld.rand() * (989 - 454);
			Xb._12 = _11;
			ctx.T(Xb.Ab, Xb._7);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_13, Xb._9);
			ctx.u(_13, _13, Qb);
			ctx.c(_13, _13, Xb._7);
			ctx.T(Xb._7, _13);
			ctx.T(Xb.Ab, Xb._7);
			_14 = 6;
			_15 = (Xb._ / _14);
			_16i0=(_15<0?0:(_15>1?1:_15));
			_16i0<0.5?ctx.V(_16s0,0,(_16i0-0)*2):ctx.V(_16s0,1,(_16i0-0.5)*2);
			_16 = Db.nb(Bd._16[0][_16s0[0]],_16s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._10;
			ctx.V(Xb.Nd,Xb._12,Xb._12);
			ctx.T(Xb.gf,[1,0.56,0.5]);
			Xb.Od = _16;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_14 = 6;
			if (Xb._ > _14) return true;
			return false;
		}


	}

	this.qd = function(Ld) {
		Ld.Dd = 0.0333333;
		Ld.Ud = 0;
		Ld.rand = function() { return Math.random(); };
		Ld.pd(new Emitter_glimmer());
		Ld.pd(new Emitter_bgFluff());
	}
		this.kb = function (v) { 			return (v < 0) ? 0 : ((v > 1) ? 1 : v); 		}

		this.lb = function (r, path, je) { 			var indexInt = Math.floor(je); 			var lerp = je - indexInt; 			ctx.Y(r, path[indexInt], path[indexInt + 1], lerp); 		}

		this.nb = function(funcValues, je) { 			var indexInt = Math.floor(je); 			var nextInt = indexInt + 1; 			return ctx.X(funcValues[indexInt], funcValues[nextInt], je - indexInt); 		}


}

return splashGlimmerL;
}));