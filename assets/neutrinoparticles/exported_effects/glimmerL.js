// 7066d8e9-69ba-46f1-853e-affff17f031f


(function (root, factory) {
    if (typeof exports === 'object' && typeof module !== 'undefined') {
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) {
        define(['exports'], function (exports) {
            (root.NeutrinoEffect = exports)['glimmerL'] = factory();
        });
    } else {
        var namespace = (root.NeutrinoEffect || (root.NeutrinoEffect = {}));
        namespace.__last__ = namespace['glimmerL'] = factory();
    }
}(typeof self !== 'undefined' ? self : this, function () {

function glimmerL(ctx) {
	var Db = this;

	var ne = function (Ld, Bd) {
		this.Ld = Ld;
		this.Bd = Bd;

		if (this.Bd.we.pe.length > 0) {
			this.we = this.Bd.we.pe[0];

			this.Lc = [ne.prototype.Ec,
				ne.prototype.Fc][this.we.xe];
		}
		else
			this.we = null;
	}

	ne.prototype = {
		Ec: function (fe, Ab, Xb) {
			var Gc = ctx.ib(Xb.Md);
			var Hc = Math.cos(Gc);
			var Ic = Math.sin(Gc);
			var ye = ctx.Ae(Xb.Nd[0]);
			var ze = ctx.Ae(Xb.Nd[1]);
			fe./**/transform(ye * Hc, ye * Ic, ze * -Ic, ze * Hc, Ab[0], Ab[1]);
		},

		Fc: function (fe, Ab, Xb) {
			var q = Xb.Mc;
			var z2 = 2.0 * q[2] * q[2];
			var xy = 2.0 * q[0] * q[1];
			var wz = 2.0 * q[3] * q[2];
			var ye = ctx.Ae(Xb.Nd[0]);
			var ze = ctx.Ae(Xb.Nd[1]);
			fe./**/transform(
				ye * (1.0 - 2.0 * q[1] * q[1] - z2),
				ye * (xy + wz),
				ze * (wz - xy),
				ze * (2.0 * q[0] * q[0] + z2 - 1.0),
				Ab[0], Ab[1]);
		},

		Pc: function (fe, Xb, ge) {
			Xb.vc(fe, -1, ge);

			if (this.we) {

				if (this.Be != null && !Xb.oc) {

					if (Xb.Od > 0.001) {
						var De = Math.floor(Xb.Qc % this.we.Rc);
						var Ee = Math.floor(Xb.Qc / this.we.Rc);

						var Ab = Xb.Ab.slice();
						var Nd = Xb.Nd.slice();
						if (!ge || ge./**/transform(Ab, Nd)) {

							var df = Math.abs(Nd[0]);
							var ef = Math.abs(Nd[1]);

							if (df > 0.001 && ef > 0.001) {
								fe.save();
								this.Lc(fe, Ab, Xb);

								fe.translate(-df * Xb.Pd[0], -ef * (1 - Xb.Pd[1]));
								fe.globalAlpha = Xb.Od;

								if (Xb.gf[0] < 0.999 || Xb.gf[1] < 0.999 || Xb.gf[2] < 0.999) {
									if (df >= 1 && ef >= 1) {
										var Ye = df < this.Tc ? df : this.Tc;
										var Ze = ef < this.Uc ? ef : this.Uc;

										ctx.af(Ye, Ze);

										ctx.bf.globalCompositeOperation = "copy";
										ctx.bf.drawImage(this.Be.image,
											this.Be.x + this.Tc * De, this.Be.y + this.Uc * Ee,
											this.Tc, this.Uc,
											0, 0, Ye, Ze);

										ctx.bf.globalCompositeOperation = "multiply";
										ctx.bf.fillStyle = ctx.ff(Xb.gf);
										ctx.bf.fillRect(0, 0, Ye, Ze);

										ctx.bf.globalCompositeOperation = "destination-atop";
										ctx.bf.drawImage(this.Be.image,
											this.Be.x + this.Tc * De, this.Be.y + this.Uc * Ee,
											this.Tc, this.Uc,
											0, 0, Ye, Ze);

										fe.drawImage(ctx.cf, 0, 0, Ye, Ze, 0, 0, df, ef);
									}
								}
								else {
									fe.drawImage(this.Be.image,
										this.Be.x + this.Tc * De, this.Be.y + this.Uc * Ee,
										this.Tc, this.Uc, 0, 0, df, ef);
								}

								fe.restore();
							}
						}
					}
				}
			}

			Xb.vc(fe, 1, ge);
		},

		Hd: function (fe, ge) {
			fe.save();

			if (this.we) {
				fe.globalCompositeOperation = this.Ld.materials[this.Ld./**/model.renderStyles[this.we.renderStyleIndex].materialIndex];
				this.Be = this.Ld.textureDescs[this.Ld./**/model.renderStyles[this.we.renderStyleIndex].textureIndices[0]];
			}
			else {
				this.Be = null;
			}

			if (this.Be) {
				this.Tc = this.Be.width / this.we.Rc;
				this.Uc = this.Be.height / this.we.Sc;
			}

			function kd(a, b) {
				if (a.Ab[2] < b.Ab[2])
					return 1;
				if (a.Ab[2] > b.Ab[2])
					return -1;
				return 0;
			}

			switch (this.Bd.Vc) {
				case 0:
					for (var Wb = 0; Wb < this.Bd.tc.length; ++Wb) {
						this.Pc(fe, this.Bd.tc[Wb], ge);
					}
					break;
				case 1:
					for (var Wb = this.Bd.tc.length; Wb-- > 0;) {
						this.Pc(fe, this.Bd.tc[Wb], ge);
					}
					break;
				case 2:
					this.Bd.tc.sort(kd);

					for (var Wb = 0; Wb < this.Bd.tc.length; ++Wb) {
						this.Pc(fe, this.Bd.tc[Wb], ge);
					}
					break;
			}

			fe.restore();
		}
	}

	var oe = function (Ld, Bd) {

		this.Ld = Ld;
		this.Bd = Bd;

		if (this.Bd.we.pe.length > 0)
			this.we = this.Bd.we.pe[0];
		else
			this.we = null;

		this.vertex = [
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] },
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] },
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] },
			{ /**/position: [0.0, 0.0, 0.0], /**/color: [0, 0, 0, 0], /**/texCoords: [[0.0, 0.0]] }];
	}

	oe.prototype = {
		qe: function (Xb, se, re, te, renderBuffer) {
			Xb.Ce(-1, se, re, te, renderBuffer);

			if (this.we) {

				if (!Xb.oc) {

					var v0 = this.vertex[0];
					var v1 = this.vertex[1];
					var v2 = this.vertex[2];
					var v3 = this.vertex[3];

					var Fe = [], Ge = [];

					if (this.we.xe == 0) {
						var a = ctx.ib(Xb.Md);
						var s = -Math.sin(a);
						var c = Math.cos(a);

						Fe[0] = se[0] * c + re[0] * s;
						Fe[1] = se[1] * c + re[1] * s;
						Fe[2] = se[2] * c + re[2] * s;

						Ge[0] = -se[0] * s + re[0] * c;
						Ge[1] = -se[1] * s + re[1] * c;
						Ge[2] = -se[2] * s + re[2] * c;
					}
					else {
						var q = Xb.Mc;
						var z2 = 2.0 * q[2] * q[2];
						var xy = 2.0 * q[0] * q[1];
						var wz = 2.0 * q[3] * q[2];

						Fe[0] = 1.0 - 2.0 * q[1] * q[1] - z2;
						Fe[1] = xy + wz;
						Fe[2] = 2.0 * q[0] * q[2] - 2.0 * q[3] * q[1];

						Ge[0] = xy - wz;
						Ge[1] = 1.0 - 2.0 * q[0] * q[0] - z2;
						Ge[2] = 2.0 * q[1] * q[2] + 2.0 * q[3] * q[0];
					}

					var He = [], Ie = [], Je = [], Ke = [];
					ctx.u(He, Fe, -Xb.Nd[0] * Xb.Pd[0]);
					ctx.u(Ie, Fe, Xb.Nd[0] * (1.0 - Xb.Pd[0]));
					ctx.u(Je, Ge, -Xb.Nd[1] * Xb.Pd[1]);
					ctx.u(Ke, Ge, Xb.Nd[1] * (1.0 - Xb.Pd[1]));

					ctx.c(v0./**/position, He, Je);
					ctx.c(v0./**/position, v0./**/position, Xb.Ab);
					ctx.c(v1./**/position, He, Ke);
					ctx.c(v1./**/position, v1./**/position, Xb.Ab);
					ctx.c(v2./**/position, Ie, Ke);
					ctx.c(v2./**/position, v2./**/position, Xb.Ab);
					ctx.c(v3./**/position, Ie, Je);
					ctx.c(v3./**/position, v3./**/position, Xb.Ab);

					{
						var rgb = ctx.v(Xb.gf, 255);
						v0./**/color = v1./**/color = v2./**/color = v3./**/color = [rgb[0], rgb[1], rgb[2], Xb.Od * 255];
					}

					{
						var De = Math.floor(Xb.Qc % this.we.Rc);
						var Ee = Math.floor(Xb.Qc / this.we.Rc);

						var Pe, Qe, Re, Se;

						var We = this.Ld.texturesRemap[this.Ld./**/model.renderStyles[this.we.renderStyleIndex].textureIndices[0]];
						if (We) {
							var Ue = We.width / this.we.Rc;
							var Ve = We.height / this.we.Sc;

							var Pe = We.x + De * Ue;
							var Qe = Pe + Ue;
							var Re = (We.y + We.height - Ee * Ve);
							var Se = Re - Ve;
						} else {
							var Ue = 1.0 / this.we.Rc;
							var Ve = 1.0 / this.we.Sc;

							var Pe = De * Ue;
							var Qe = Pe + Ue;
							var Re = (1.0 - Ee * Ve);
							var Se = Re - Ve;
						}

						v0./**/texCoords[0] = [Pe, Se];
						v1./**/texCoords[0] = [Pe, Re];
						v2./**/texCoords[0] = [Qe, Re];
						v3./**/texCoords[0] = [Qe, Se];
					}

					if (renderBuffer.beforeQuad) {
						renderBuffer.beforeQuad(this.we.renderStyleIndex);
					}

					renderBuffer.pushVertex(v0);
					renderBuffer.pushVertex(v1);
					renderBuffer.pushVertex(v2);
					renderBuffer.pushVertex(v3);

					if (!renderBuffer.__lastRenderCall) {
						renderBuffer.__lastRenderCall = new ctx.RenderCall(0, 6, this.we.renderStyleIndex);
					} else {
						var lastRenderCall = renderBuffer.__lastRenderCall;

						if (lastRenderCall.renderStyleIndex == this.we.renderStyleIndex) {
							lastRenderCall.numIndices += 6;
						} else {
							renderBuffer.pushRenderCall(lastRenderCall);
							renderBuffer.__lastRenderCall = new ctx.RenderCall(
								lastRenderCall.startIndex + lastRenderCall.numIndices,
								6, this.we.renderStyleIndex);
						}
					}
				}
			}

			Xb.Ce(1, se, re, te, renderBuffer);
		},

		ue: function (se, re, te, renderBuffer) {
			switch (this.Bd.Vc) {
				case 0:
					for (var Wb = 0; Wb < this.Bd.tc.length; ++Wb) {
						this.qe(this.Bd.tc[Wb], se, re, te, renderBuffer);
					}
					break;

				case 1:
					for (var Wb = this.Bd.tc.length; Wb-- > 0;) {
						this.qe(this.Bd.tc[Wb], se, re, te, renderBuffer);
					}
					break;

				case 2:
					this.Bd.tc.forEach(function (Xb) {
						Xb.depth = ctx.H(te, Xb.Ab);
					});

					this.Bd.tc.sort(function (a, b) {
						if (a.depth < b.depth)
							return 1;
						if (a.depth > b.depth)
							return -1;
						return 0;
					});

					this.Bd.tc.forEach(function (Xb) {
						this.qe(Xb, se, re, te, renderBuffer);
					}, this);
					break;
			}
		}
	}

	var ld = function (Ld, we, ve) {
		var Vb = this;
		this.Ld = Ld;
		this.we = we;

		// Eb

		function Eb() {
			this.Fb = 0;
			this.Gb = 1;
			this.Hb = null;
			this.Ib = null;
			this.Kb = 0;
			this.Lb = 1;

			Vb.we.Mb(this); // IMPL

			this.Nb = function () {
				this.Ob = this.Gb;
				this.Fb = 0;
			}

			this.Nb();
		}

		Eb.prototype = {
			Jd: function () {
				this.Nb();
			},

			Id: function (Qb, Ab, Mc) {
				Vb.we.Pb(Qb, Vb, this); // IMPL

				var Rb = Vb.Rb;
				var systemTime = Ld.Rb;
				var Sb = Qb;
				var ic = 0;

				if (this.zb > 0.000001) {

					var Tb = this.Ob + Qb * this.zb;

					while (Tb >= 1.0) {
						var Ub = this.zb < 0.001 ? 0.0 : (1.0 - this.Ob) / this.zb;
						Sb -= Ub;
						Rb += Ub;
						systemTime += Ub;

						if (this.Hb != null && Rb > this.Hb) {
							Vb.disactivate();
							break;
						}

						Vb.Rb = Rb;
						Ld.Rb = systemTime;

						if (Ab && Qb > 0)
							ctx.ab(Vb.Ab, Ab, Vb.Bb, Sb / Qb);

						if (Mc && Qb > 0)
							ctx.slerpq(Vb.Mc, Mc, Vb.prevRotation, Sb / Qb);

						// for the future when Jb would be external
						this.Lb = this.Jb;

						for (var Wb = 0; Wb < this.Jb; ++Wb) {
							if (Vb.sc.length == 0)
								break;

							if (this.Jb == 1)
								this.Kb = 0;
							else
								this.Kb = Wb / (this.Jb - 1);

							var Xb = Vb.sc.pop();
							Vb.tc.unshift(Xb);

							if (Wb == 0)
								Xb.Yb();
							else
								Xb.Zb();

							Xb.Id(Sb);
							++ic;
						}

						this.Ob = 0.0;
						Tb -= 1.0;

						if (this.Ib != null && ++this.Fb >= this.Ib) {
							Vb.disactivate();
							break;
						}
					}

					this.Ob = Tb;
				}
				Rb += Sb;
				Vb.Rb = Rb;

				if (Ab)
					ctx.T(Vb.Ab, Ab);

				if (Mc)
					ctx.U(Vb.Mc, Mc);

				return ic;
			}
		}

		// ac

		function ac() {
			this.Gb = 1;
			this.Kb = 0;
			this.Lb = 1;

			Vb.we.Mb(this); // IMPL

			this.Nb = function () {
				this.bc = this.Gb;
			}

			this.Nb();
		}

		ac.prototype = {
			Jd: function () {
				this.Nb();
			},

			Id: function (Qb, Ab, Mc) {
				Vb.we.Pb(Qb, Vb, this); // IMPL

				var cc = Vb.Rb;
				var dc = cc + Qb;
				var systemTimeBeforeFrame = Ld.Rb;
				var systemTimeAfterFrame = systemTimeBeforeFrame + Qb;
				var ec = Ab ? ctx.O(ctx.h(Ab, Vb.Bb)) : 0;
				var ic = 0;

				if (ec > 0.000001) {
					var fc = ec / this.rd;
					var Tb = this.bc + fc;

					var hc = fc < 0.001 ?
						1.0 - this.bc : (1.0 - this.bc) / fc;

					var jc = [];

					while (Tb > 1.0) {
						var kc = cc + hc * Qb;

						if (Ab)
							ctx.ab(jc, Vb.Bb, Ab, hc);

						Vb.Rb = kc;
						ctx.T(Vb.Ab, jc);
						Ld.Rb = ctx.X(systemTimeBeforeFrame, systemTimeAfterFrame, hc);

						// for the future when Jb would be external
						this.Lb = this.Jb;

						for (var Wb = 0; Wb < this.Jb; ++Wb) {
							if (Vb.sc.length == 0)
								break;

							if (this.Jb == 1)
								this.Kb = 0;
							else
								this.Kb = Wb / (this.Jb - 1);

							var Xb = Vb.sc.pop();
							Vb.tc.unshift(Xb);

							if (Wb == 0)
								Xb.Yb();
							else
								Xb.Zb();

							Xb.Id(Qb * (1.0 - hc));
							++ic;
						}

						hc += 1.0 / fc;
						Tb -= 1.0;
					}

					this.bc = Tb;
				}

				Vb.Rb = dc;

				if (Ab)
					ctx.T(Vb.Ab, Ab);

				if (Mc)
					ctx.U(Vb.Mc, Mc);

				return ic;
			}
		}

		// mc

		function mc() {
			this.Ab = [];
			this.Pd = [];
			this.Nd = [];
			this.gf = [];
			this.Kc = [];
		}

		mc.prototype = {
			nc: function () {
				this.oc = false;

				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];
					pc.Bd.Jd(this.Ab, null);

					if (pc.Ad.sd)
						pc.Bd.disactivate();
				}
			},

			Yb: function () {
				Vb.we.fd(Vb, this); // IMPL
				this.nc();
			},

			Zb: function () {
				Vb.we.gd(Vb, this); // IMPL
				this.nc();
			},

			Id: function (Qb) {
				Vb.we.qc(Qb, Vb, this); // IMPL

				this.rc(Qb);
			},

			pc: function (je) {
				return this.Kc[je].Bd;
			},

			rc: function (Qb) {
				for (var i = 0; i < this.Kc.length; i++) {
					this.Kc[i].Bd.Id(Qb, this.Ab, null);
				}
			},

			uc: function (md, nd) {
				this.Kc.push({
					Bd: new ld(Ld, md, ve),
					Ad: nd
				});
			},

			vc: function (fe, xc, ge) {
				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];

					if (xc == pc.Ad.xc)
						pc.Bd.Hd(fe, ge);
				}
			},

			Ce: function (xc, se, re, te, renderBuffer) {
				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];

					if (xc == pc.Ad.xc)
						pc.Bd.ue(se, re, te, renderBuffer);
				}
			},

			wc: function (fe) {
				this.oc = true;
				for (var i = 0; i < this.Kc.length; ++i) {
					var pc = this.Kc[i];

					if (pc.Ad.sd)
						pc.Bd.activate();
					else
						pc.Bd.disactivate();
				}
			},

			yc: function (Gd) {
				for (var i = 0; i < this.Kc.length; ++i) {
					this.Kc[i].Bd.Ed(Gd);
				}
			}
		}

		// zc

		function zc() {
		}

		zc.prototype.Ac = function (Xb) {
			return Vb.we.Cc(Vb, Xb, this); // IMPL
		}

		// ld Ad

		this.Ab = [];
		this.Bb = [];
		this.Mc = [];
		this.prevRotation = [];
		this.tc = [];
		this.sc = [];
		this.Wc = new zc();
		this.construct = new ve(this.Ld, this);
		this.Yc = [];
		this.ad = [];

		this.dd = function () {
			this.vd = new Eb();
		}

		this.ed = function () {
			this.vd = new ac();
		}

		this.we.ud(this); // IMPL

		for (var Wb = 0; Wb < this.jd; ++Wb) {
			var Xb = new mc();

			for (var id = 0; id < this.Yc.length; ++id) {
				var hd = this.Yc[id];
				Xb.uc(hd.Db, hd.Ad);
			}

			this.sc.push(Xb);
		}

		this.Nb = function (Ab, Mc) {

			ctx.T(this.Ab, Ab ? Ab : [0, 0, 0]);
			ctx.T(this.Bb, this.Ab);
			ctx.U(this.Mc, Mc ? Mc : [0, 0, 0, 1]);
			ctx.U(this.prevRotation, this.Mc);

			this.Rb = 0.0;
			this.wd = 0.0;
			this.Zc = true;
			this.paused_ = false;
			this.generatorsPaused_ = false;
			ctx.W(this.ad, 0, 0, 0);
		}
	}

	ld.prototype.Jd = function (Ab, Mc) {
		this.Nb(Ab, Mc);

		this.sc.push.apply(this.sc, this.tc);
		this.tc.length = 0;

		this.vd.Jd();
	}

	ld.prototype.Id = function (Qb, Ab, Mc) {

		if (this.paused_)
		{
			this.Td(Ab, Mc);
			return;
		}

		this.wd = this.Rb;

		if (Ab) {
			ctx.T(this.Bb, this.Ab);
			if (Qb > 0.0001) {
				var shift = [];
				ctx.g(shift, Ab, this.Bb);
				ctx.T(this.ad, shift);
				ctx.w(this.ad, this.ad, Qb);
			}
			else {
				ctx.W(this.ad, 0, 0, 0);
			}
		}
		else {
			ctx.W(this.ad, 0, 0, 0);
		}

		if (Mc)
		{
			ctx.U(this.prevRotation, this.Mc);
		}

		var ic;

		if (this.Zc && !this.generatorsPaused_) {
			ic = this.vd.Id(Qb, Ab, Mc);
		}
		else {
			if (Ab)
				ctx.T(this.Ab, Ab);

			if (Mc)
				ctx.U(this.Mc, Mc);

			ic = 0;
			this.Rb += Qb;
		}

		for (var Wb = ic; Wb < this.tc.length;) {
			var Xb = this.tc[Wb];

			if (!Xb.oc) {
				Xb.Id(Qb);

				if (this.Wc.Ac(this.tc[Wb])) {
					Xb.wc();

					if (this.xd(Wb))
						continue;
				}
			}
			else {
				Xb.rc(Qb);

				if (this.xd(Wb))
					continue;
			}

			++Wb;
		}
	};

	ld.prototype.xd = function (je) {
		var Xb = this.tc[je];

		var ready = true;

		for (var id = 0; id < Xb.Kc.length; ++id) {
			var Bd = Xb.Kc[id].Bd;

			if (Bd.activated() || Bd.tc.length > 0) {
				ready = false;
				break;
			}
		}

		if (ready) {
			this.sc.push(this.tc[je]);
			this.tc.splice(je, 1);
			return true;
		}

		return false;
	}

	ld.prototype.Hd = function (fe, ge) {
		this.construct.Hd(fe, ge);
	}

	ld.prototype.ue = function (se, re, te, renderBuffer) {
		this.construct.ue(se, re, te, renderBuffer);
	}

	ld.prototype.Td = function (Ab, Mc) {
		this.wd = this.Rb;

		if (Ab) {
			ctx.T(this.Bb, this.Ab);
			ctx.T(this.Ab, Ab);
		}

		if (Mc) {
			ctx.U(this.prevRotation, this.Mc);
			ctx.U(this.Mc, Mc);
		}
	}

	ld.prototype.uc = function (md, nd) {
		this.Yc.push({ Db: md, Ad: nd });
	}

	ld.prototype./**/pause = function () {
		this.paused_ = true;
	}

	ld.prototype./**/unpause = function () {
		this.paused_ = false;
	}

	ld.prototype./**/paused = function () {
		return this.paused_;
	}

	ld.prototype./**/pauseGenerators = function () {
		this.generatorsPaused_ = true;
	}

	ld.prototype./**/unpauseGenerators = function () {
		this.generatorsPaused_ = false;
	}

	ld.prototype./**/generatorsPaused = function () {
		return this.generatorsPaused_;
	}

	ld.prototype.activate = function () {
		this.Zc = true;
	}

	ld.prototype.disactivate = function () {
		this.Zc = false;
	}

	ld.prototype.activated = function () {
		return this.Zc;
	}

	ld.prototype./**/getNumParticles = function () {
		return this.tc.length;
	}

	var ke = function () {
		var Cb = this;

		this._init = function (we, Ab, Mc, ve, options) {
			this./**/model = we;

			this.Ab = [];
			this.Mc = [];

			// ke Ad

			this.od = [];

			this.pd = function (md) {
				var Bd = new ld(this, md, ve);
				Bd.Nb(this.Ab, this.Mc);
				this["_".concat(md.name)] = Bd;
				this.od.push(Bd);
			}

			this.Nb = function (Ab, Mc) {
				this.Cd = 0.0;
				this.Rb = 0.0;
				ctx.T(this.Ab, Ab ? Ab : [0, 0, 0]);
				ctx.U(this.Mc, Mc ? Mc : [0, 0, 0, 1]);
			}

			this.Nb(Ab, Mc);
			this./**/model.qd(this); // IMPL

			this._presimNeeded = true;

			if (options.paused) {
				this./**/pauseAllEmitters();
			} else {
				this.zeroUpdate();
				this./**/update(this.Ud, Ab, Mc);
				this._presimNeeded = false;
			}

			if (options.generatorsPaused) {
				this./**/pauseGeneratorsInAllEmitters();
			}
		}
	}

	ke.prototype./**/restart = function (/**/position, /**/rotation) {

		this.Nb(/**/position ? /**/position : this.Ab, /**/rotation ? /**/rotation : this.Mc);

		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Jd(this.Ab, this.Mc);
		}

		this.zeroUpdate();

		this./**/update(this.Ud, this.Ab, this.Mc);
		this._presimNeeded = false;
	}

	ke.prototype.zeroUpdate = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Id(0, this.Ab, this.Mc);
		}
	}

	ke.prototype./**/update = function (/**/dt, /**/position, /**/rotation) {
		var updatedTime = 0.0;
		var hc = [];
		ctx.T(hc, this.Ab);
		var frameRotation = [];
		ctx.U(frameRotation, this.Mc);

		if (/**/position && ctx.equalv3_(/**/position, this.Ab))
			/**/position = null;

		if (/**/rotation && ctx.equalq_(/**/rotation, this.Mc))
			/**/rotation = null;

		while ((/**/dt - updatedTime) + this.Cd >= this.Dd) {
			var cc = this.Rb;

			if (/**/position)
				ctx.ab(hc, this.Ab, /**/position, updatedTime / /**/dt);

			if (/**/rotation)
				ctx.slerpq(frameRotation, this.Mc, /**/rotation, updatedTime / /**/dt);

			for (var i = 0; i < this.od.length; ++i) {
				this.od[i].Id(this.Dd, hc, frameRotation);

				this.Rb = cc;
			}

			updatedTime += this.Dd - this.Cd;
			this.Cd = 0.0;
			this.Rb = cc + this.Dd;
		}

		if (/**/position)
			ctx.T(this.Ab, /**/position);

		if (/**/rotation)
			ctx.U(this.Mc, /**/rotation);

		this.Cd += /**/dt - updatedTime;
	}

	ke.prototype./**/resetPosition = function (/**/position, /**/rotation) {

		if (/**/position)
			ctx.T(this.Ab, /**/position);

		if (/**/rotation)
			ctx.U(this.Mc, /**/rotation);

		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Td(this.Ab, this.Mc);
		}
	}

	ke.prototype./**/setPropertyInAllEmitters = function (/**/name, /**/value) {
		var propName = "_".concat(/**/name);

		if (/**/value instanceof Array) {
			if (/**/value.length == 2) {
				for (var i = 0; i < this.od.length; ++i) {
					ctx.S(this.od[i][propName], /**/value);
				}
			}
			else {
				for (var i = 0; i < this.od.length; ++i) {
					ctx.T(this.od[i][propName], /**/value);
				}
			}
		}
		else {
			for (var i = 0; i < this.od.length; ++i) {
				this.od[i][propName] = /**/value;
			}
		}
	}

	ke.prototype./**/pauseAllEmitters = function() {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/pause();
		}
	}

	ke.prototype./**/unpauseAllEmitters = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/unpause();
		}
		this.zeroUpdate();

		if (this._presimNeeded) {
			this./**/update(this.Ud, this.Ab, this.Mc);
			this._presimNeeded = false;
		}
	}

	ke.prototype./**/areAllEmittersPaused = function () {
		for (var i = 0; i < this.od.length; ++i) {
			if (!this.od[i].paused())
				return false;
		}
		return true;
	}

	ke.prototype./**/pauseGeneratorsInAllEmitters = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/pauseGenerators();
		}
	}

	ke.prototype./**/unpauseGeneratorsInAllEmitters = function () {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i]./**/unpauseGenerators();
		}
	}

	ke.prototype./**/areGeneratorsInAllEmittersPaused = function () {
		for (var i = 0; i < this.od.length; ++i) {
			if (!this.od[i].generatorsPaused())
				return false;
		}
		return true;
	}

	ke.prototype./**/getNumParticles = function() {
		var numParticles = 0;

		for (var i = 0; i < this.od.length; ++i) {
			numParticles += this.od[i].getNumParticles();
		}

		return numParticles;
	}


	var le = function () {
		this._init = function (we, Ab, Mc, renderBuffer, options) {
			le.prototype._init.call(this, we, Ab, Mc, oe, options);

			this.texturesRemap = [];

			var indices = [];

			{
				var verDisp;
				for (var Wb = 0; Wb < this./**/model.Xe; ++Wb) {
					verDisp = Wb * 4;
					indices.push(verDisp + 0, verDisp + 3, verDisp + 1, verDisp + 1, verDisp + 3, verDisp + 2);
				}
			}

			this.renderBuffer = renderBuffer;
			this.renderBuffer.initialize(this./**/model.Xe * 4, [2], indices, this./**/model.Xe);
			this.renderBuffer.__numIndices = 0;
		}
	}

	le.prototype = new ke();

	le.prototype./**/fillGeometryBuffers = function (/**/cameraRight, /**/cameraUp, /**/cameraDir) {
		this.renderBuffer.cleanup();
		this.renderBuffer.__lastRenderCall = null;

		this.od.forEach(function (Bd) {
			Bd.ue(/**/cameraRight, /**/cameraUp, /**/cameraDir, this.renderBuffer);
		}, this);

		if (this.renderBuffer.__lastRenderCall)
			this.renderBuffer.pushRenderCall(this.renderBuffer.__lastRenderCall);
	}

	var me = function () {
		this._init = function (we, Ab, Mc, options) {
			me.prototype._init.call(this, we, Ab, Mc, ne, options);

			this.materials = [];
			this./**/model.materials.forEach(function (value) {
				this.materials.push(['source-over', 'lighter', 'multiply'][value]);
			}, this);

			this./**/textureDescs = [];
		}
	}

	me.prototype = new ke();

	me.prototype./**/draw = function (/**/context, /**/camera) {
		for (var i = 0; i < this.od.length; ++i) {
			this.od[i].Hd(/**/context, /**/camera);
		}
	}

	this.createWGLInstance = function (/**/position, /**/rotation, /**/renderBuffer, /**/options) {
		var Ld = new le();
		Ld._init(this, /**/position, /**/rotation, /**/renderBuffer, /**/options || {});
		return Ld;
	}

	this.createCanvas2DInstance = function (/**/position, /**/rotation, /**/options) {
		var Ld = new me();
		Ld._init(this, /**/position, /**/rotation, /**/options || {});
		return Ld;
	}
	this.textures = ['glow_point_red_gold.png'];
	this.materials = [1];
	this.renderStyles = [{materialIndex:0,textureIndices:[0]}];
	this.Xe = 900;

	function Emitter_glimmer() {

		var _2s = [[9.95946, 9.80645], [19.9189, 9.80645], [19.9189, 19.6129], [9.95946, 19.6129], [49.7973, 39.2258], [49.7973, 78.4516], [19.9189, 39.2258], [29.8784, 19.6129], [29.8784, 39.2258], [39.8378, 19.6129], [89.6351, 78.4516], [39.8378, 78.4516], [39.8378, 39.2258]], _1, _2Srch, _2 = [], _3 = [], _5, _7, _9, _10, _11, _12, _12i0, _12s0 = [], _13;
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmer";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._12 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 4;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 4;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2Srch = this._2f(_1 * 339840);
			ctx.yb(_2, -945 + (_2Srch[0] % 148) * 9.95946, 156 + Math.floor(_2Srch[0] / 148) * 9.80645, _2s[_2Srch[1]], Bd.Ld.rand);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			_5 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._6 = _5;
			_7 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._8 = _7;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2Srch = this._2f(_1 * 339840);
			ctx.yb(_2, -945 + (_2Srch[0] % 148) * 9.95946, 156 + Math.floor(_2Srch[0] / 148) * 9.80645, _2s[_2Srch[1]], Bd.Ld.rand);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			_5 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._6 = _5;
			_7 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._8 = _7;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			_9 = Xb._6 + Qb * 33;
			Xb._6 = _9;
			ctx.T(Xb.Ab, Xb._4);
			_10 = 2;
			_11 = (Xb._ / _10);
			_12i0=(_11<0?0:(_11>1?1:_11));
			_12i0<0.204609?ctx.V(_12s0,0,(_12i0-0)*4.88738):ctx.V(_12s0,1,(_12i0-0.204609)*1.25724);
			_12 = Db.nb(Bd._12[0][_12s0[0]],_12s0[1]);
			_13 = (Xb._8 * _12);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._6;
			ctx.V(Xb.Nd,_13,_13);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = 1;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_10 = 2;
			if (Xb._ > _10) return true;
			return false;
		}

			this._2f = function(i) {
				return i<146295?i<77603?i<23219?i<16958?i<7036?i<2224?i<454?i<155?[310,0]:i<201?[457,0]:[458,0]:i<458?[1048,0]:i<1250?i<828?i<574?[605,0]:[606,0]:i<996?[753,0]:[754,0]:i<1724?i<1469?[901,0]:[902,0]:i<1970?[1049,0]:[1050,0]:i<3870?i<3143?i<2635?i<2443?[311,0]:[312,0]:[459,1]:i<3366?i<3307?[313,0]:[314,0]:i<3620?[461,0]:[462,0]:i<5178?i<4583?i<4248?i<4124?[607,0]:[608,0]:i<4502?[755,0]:[756,0]:i<4998?i<4744?[609,0]:[610,0]:[758,0]:i<6159?i<5658?i<5433?[903,0]:[904,0]:i<5905?[1051,0]:[1052,0]:i<6528?i<6284?[905,0]:[906,0]:[1053,1]:i<11433?i<9529?i<7782?i<7436?i<7150?[1343,0]:i<7182?[1196,0]:[1344,0]:i<7533?[1491,0]:i<7779?[1492,0]:[1640,0]:i<8781?i<8286?i<8035?[1197,0]:[1198,0]:i<8540?[1345,0]:[1346,0]:i<9289?[1493,1]:i<9370?[1641,0]:[1642,0]:i<10123?i<9545?[1936,0]:i<9675?i<9549?[2083,0]:[2231,0]:i<9868?[2084,0]:[2232,0]:i<10567?i<10139?[1790,0]:i<10317?[1937,0]:[1938,0]:i<11076?i<10822?[2085,0]:[2086,0]:i<11328?[2233,0]:[2234,0]:i<14470?i<12741?i<11760?i<11603?i<11451?[1199,0]:[1200,0]:i<11692?[1347,0]:[1348,0]:i<12236?i<12014?[1201,0]:[1202,0]:i<12488?[1349,0]:[1350,0]:i<13677?i<13249?[1495,1]:i<13441?[1643,0]:[1644,0]:i<14181?i<13931?[1497,0]:[1498,0]:i<14388?[1645,0]:[1646,0]:i<15401?i<15076?i<14566?i<14524?[1791,0]:[1792,0]:[1939,1]:i<15080?[1793,0]:i<15307?[1941,0]:[1942,0]:i<15977?i<15909?[2087,1]:i<15931?[2235,0]:[2236,0]:i<16486?i<16232?[2089,0]:[2090,0]:i<16703?[2237,0]:[2238,0]:i<20888?i<19428?i<18864?i<17957?i<17447?i<17193?i<16960?[2378,0]:[2379,0]:i<17199?[2526,0]:[2527,0]:[2380,3]:i<18354?i<18189?[2675,0]:[2823,0]:[2676,3]:i<19072?i<19001?[2381,0]:[2529,0]:i<19176?[2677,0]:i<19390?[2825,0]:[2826,0]:i<19802?i<19481?[2971,0]:i<19731?[2972,0]:[3120,0]:i<20805?i<20312?[2973,1]:i<20550?[3121,0]:[3122,0]:i<20823?[3269,0]:[3270,0]:i<21379?i<21177?i<21171?i<20938?[2385,0]:[2386,0]:[2534,0]:i<21263?i<21201?[2827,0]:[2828,0]:i<21358?[2829,0]:[2830,0]:i<23147?i<22397?i<21889?[2975,1]:i<22144?[3123,0]:[3124,0]:i<22821?i<22652?[2977,0]:[2978,0]:i<23049?[3125,0]:[3126,0]:i<23197?[3271,0]:[3272,0]:i<54336?i<36871?i<30307?i<27928?i<23786?i<23429?i<23384?i<23378?[463,0]:[464,0]:[465,0]:i<23644?[466,0]:[467,0]:i<25896?i<24909?i<24701?i<24201?i<24040?[611,0]:[612,0]:i<24455?[759,0]:[760,0]:i<24815?[613,0]:[761,0]:i<25758?i<25398?i<25164?[907,0]:[908,0]:i<25652?[1055,0]:[1056,0]:i<25840?[909,0]:[1057,0]:i<26910?i<26402?i<26150?[614,0]:[615,0]:[762,1]:i<27420?[910,1]:[1058,1]:i<28813?i<28434?i<27962?[616,0]:i<28216?[764,0]:[765,0]:i<28465?[619,0]:i<28623?[766,0]:[767,0]:i<29323?i<29238?i<29009?[912,0]:[913,0]:i<29310?[1060,0]:[1061,0]:i<29833?[914,1]:i<30052?[1062,0]:[1063,0]:i<33138?i<32233?i<30429?i<30410?i<30377?i<30354?[1203,0]:[1351,0]:i<30409?[1205,0]:[1353,0]:[1499,0]:i<31422?i<30937?[1206,1]:i<31168?[1354,0]:[1355,0]:i<31849?i<31595?[1502,0]:[1503,0]:i<31979?[1650,0]:[1651,0]:i<32647?i<32236?[1943,0]:i<32386?[2091,0]:i<32632?[2239,0]:[2240,0]:i<32979?i<32725?[1798,0]:[1799,0]:i<32985?[1946,0]:[1947,0]:i<36220?i<34775?i<33816?i<33374?i<33237?[1208,0]:[1209,0]:i<33567?[1356,0]:[1357,0]:i<34326?[1210,1]:i<34581?[1358,0]:[1359,0]:i<35793?i<35284?i<35029?[1504,0]:[1505,0]:i<35538?[1652,0]:[1653,0]:i<36085?i<36043?[1506,0]:[1507,0]:[1654,0]:i<36859?i<36684?i<36474?[1800,0]:[1801,0]:i<36835?[1948,0]:[1949,0]:[1802,0]:i<43545?i<40488?i<39214?i<37684?i<37595?i<37112?i<37033?[620,0]:[621,0]:i<37367?[768,0]:[769,0]:[770,0]:[916,7]:i<39470?i<39331?[771,0]:[772,0]:i<39980?[919,1]:[1067,1]:i<41629?i<41261?i<40752?i<40599?[625,0]:[626,0]:i<41006?[773,0]:[774,0]:i<41340?[627,0]:i<41592?[775,0]:[776,0]:i<42649?[921,2]:i<43035?i<42904?[923,0]:[924,0]:[1071,1]:i<49448?i<46693?i<45333?i<44207?i<43908?i<43795?i<43677?[1212,0]:[1213,0]:i<43797?[1360,0]:[1361,0]:i<43984?[1214,0]:[1362,0]:i<44823?i<44497?i<44244?[1508,0]:[1509,0]:i<44568?[1656,0]:[1657,0]:[1510,3]:i<45786?i<45364?i<45351?[1215,0]:[1216,0]:i<45596?[1363,0]:[1364,0]:i<46296?[1511,1]:i<46487?[1659,0]:[1660,0]:i<47988?i<47721?i<47211?i<46999?i<46745?[1804,0]:[1805,0]:i<47001?[1952,0]:[1953,0]:[1806,3]:i<47752?[2101,0]:i<47973?[2102,0]:[2250,0]:i<48592?i<48129?i<48128?[1807,0]:[1808,0]:i<48384?[1955,0]:[1956,0]:i<49102?[2103,1]:i<49249?[2251,0]:[2252,0]:i<52560?i<50935?i<49948?i<49573?i<49481?[1217,0]:[1218,0]:i<49755?[1365,0]:[1366,0]:i<50429?i<50174?[1219,0]:[1220,0]:i<50680?[1367,0]:[1368,0]:i<51860?i<51445?[1513,1]:i<51654?[1661,0]:[1662,0]:i<52358?i<52115?[1515,0]:[1516,0]:i<52512?[1663,0]:[1664,0]:i<53181?i<53002?i<52564?[1810,0]:i<52760?[1957,0]:[1958,0]:[1959,0]:i<54041?i<53691?[2105,1]:i<53890?[2253,0]:[2254,0]:i<54281?i<54278?[2107,0]:[2108,0]:[2255,0]:i<61264?i<60020?i<56725?i<55395?i<54977?i<54582?i<54575?i<54563?[2387,0]:[2388,0]:[2535,0]:i<54861?i<54644?[2831,0]:[2832,0]:[2833,0]:i<55138?i<54987?[2390,0]:[2391,0]:i<55167?[2538,0]:[2539,0]:i<56473?i<56471?i<56116?i<55801?i<55546?[2979,0]:[2980,0]:i<55877?[3127,0]:[3128,0]:i<56335?[2981,0]:[3129,0]:[3276,0]:i<56493?[3279,0]:i<56498?[3426,0]:[3427,0]:i<59220?i<58487?i<57717?i<57218?i<56966?[2392,0]:[2393,0]:i<57462?[2540,0]:[2541,0]:i<57978?i<57908?[2394,0]:[2395,0]:i<58233?[2542,0]:[2543,0]:i<58696?i<58553?[2688,0]:[2689,0]:i<59094?i<58847?[2690,0]:[2691,0]:[2839,0]:i<59356?i<59272?[2987,0]:[3135,0]:i<59708?i<59392?[3280,0]:i<59644?[3428,0]:[3429,0]:i<59878?[3283,0]:[3431,0]:i<60374?i<60263?i<60026?[3574,0]:[3575,0]:[3723,0]:i<61254?i<61190?i<60806?i<60629?[3576,0]:[3577,0]:i<61059?[3724,0]:[3725,0]:[3579,0]:[3872,0]:i<72628?i<67755?i<63133?i<62104?i<61394?[2544,0]:i<61692?i<61646?[2692,0]:[2693,0]:i<61947?[2840,0]:[2841,0]:i<62409?i<62158?[2400,0]:i<62168?[2547,0]:[2548,0]:i<62731?i<62476?[2695,0]:[2696,0]:i<62878?[2843,0]:[2844,0]:i<65765?i<64370?i<64130?i<63620?i<63388?[2988,0]:[2989,0]:[3136,1]:i<64152?[2990,0]:[3138,0]:i<65384?i<64880?[3284,1]:i<65135?[3432,0]:[3433,0]:i<65631?[3286,0]:[3434,0]:i<66735?i<66235?i<65980?[2991,0]:[2992,0]:i<66480?[3139,0]:[3140,0]:[3287,2]:i<69802?i<68411?i<68189?i<67849?i<67847?[2401,0]:[2402,0]:i<68104?[2549,0]:[2550,0]:i<68218?[2551,0]:[2552,0]:i<68994?i<68738?i<68666?[2697,0]:[2698,0]:i<68982?[2845,0]:[2846,0]:i<69363?i<69108?[2699,0]:[2700,0]:i<69547?[2847,0]:[2848,0]:i<71193?i<70181?i<69998?i<69985?[2993,0]:[2994,0]:i<70136?[3141,0]:[3142,0]:i<70683?i<70428?[2995,0]:[2996,0]:[3143,1]:i<71608?i<71382?i<71299?[3289,0]:[3290,0]:i<71459?[3437,0]:[3438,0]:[3291,2]:i<76131?i<74490?i<73481?i<73093?i<72993?i<72858?[3580,0]:[3581,0]:i<73072?[3582,0]:[3730,0]:i<73263?[4025,0]:[4026,0]:i<74444?i<73991?[3583,1]:i<74220?[3731,0]:[3732,0]:i<74454?[3879,1]:[4027,0]:i<75526?i<75407?i<74897?i<74744?i<74497?[4172,0]:[4173,0]:[4321,0]:[4174,3]:i<75415?[4469,0]:[4470,0]:i<76032?i<75740?i<75731?[4175,0]:[4176,0]:i<75994?[4323,0]:[4324,0]:[4471,0]:i<77530?i<76610?i<76391?i<76177?[3585,0]:[3586,0]:i<76407?[3733,0]:[3734,0]:i<77119?i<76865?[3587,0]:[3588,0]:i<77374?[3735,0]:[3736,0]:i<77547?[3882,0]:[3883,0]:i<106906?i<93824?i<91040?i<78143?i<78137?i<77807?i<77718?i<77605?[925,0]:[1073,0]:[1075,0]:i<78007?[1076,0]:[1077,0]:[1078,0]:i<85096?i<82127?i<80143?i<79100?i<78638?i<78376?i<78350?[1221,0]:[1222,0]:i<78558?[1369,0]:[1370,0]:i<78845?[1223,0]:[1371,0]:i<79633?i<79333?i<79157?[1517,0]:[1518,0]:i<79378?[1665,0]:[1666,0]:[1519,3]:i<81161?i<80653?[1224,1]:i<80908?[1372,0]:[1373,0]:i<81617?i<81416?[1520,0]:[1521,0]:[1668,1]:i<84037?i<83001?i<82491?i<82315?i<82130?[1813,0]:[1814,0]:[1962,0]:[1815,3]:i<83527?i<83229?i<83003?[2109,0]:[2110,0]:i<83272?[2257,0]:[2258,0]:[2111,3]:i<84834?i<84547?[1816,1]:i<84772?[1964,0]:[1965,0]:i<85001?[2112,0]:[2260,0]:i<87841?i<86629?i<85582?i<85235?i<85177?[1226,0]:i<85231?[1374,0]:[1375,0]:i<85336?[1228,0]:[1376,0]:i<86119?i<85641?i<85605?[1522,0]:[1523,0]:i<85894?[1670,0]:[1671,0]:[1524,3]:i<87180?i<86812?i<86791?[1229,0]:[1230,0]:i<87067?[1377,0]:[1378,0]:i<87528?i<87435?[1525,0]:[1526,0]:i<87783?[1673,0]:[1674,0]:i<90348?i<89232?i<88722?i<88351?[1818,1]:i<88480?[1966,0]:[1967,0]:[1820,3]:i<89838?i<89485?i<89241?[2114,0]:[2115,0]:i<89583?[2262,0]:[2263,0]:[2116,3]:i<90825?i<90612?i<90600?[1821,0]:[1822,0]:[1969,0]:i<90967?[2117,0]:[2265,0]:i<91303?i<91043?[1975,0]:i<91154?[2123,0]:[2271,0]:i<91785?i<91547?i<91408?[1976,0]:[1977,0]:i<91665?[1978,0]:[1979,0]:i<92804?i<92294?i<92039?[2124,0]:[2125,0]:[2272,1]:[2126,2]:i<104891?i<102339?i<100267?i<95823?i<94982?i<94401?i<94187?i<94049?i<93838?[2405,0]:[2406,0]:i<94186?[2553,0]:[2554,0]:i<94400?[2407,0]:[2555,0]:i<94886?i<94661?i<94652?[2701,0]:[2702,0]:i<94885?[2849,0]:[2850,0]:[2851,0]:i<94998?[2408,0]:i<95314?i<95106?[2704,0]:[2705,0]:i<95568?[2852,0]:[2853,0]:i<98244?i<96934?i<96431?i<96021?i<95963?[2997,0]:[2998,0]:i<96188?[3145,0]:[3146,0]:i<96679?[2999,0]:[3147,0]:i<97928?i<97444?[3293,1]:i<97699?[3441,0]:[3442,0]:i<98179?[3295,0]:[3443,0]:i<99264?[3000,2]:i<99771?i<99516?[3296,0]:[3297,0]:i<100012?[3444,0]:[3445,0]:i<101525?i<101523?i<101108?i<100798?i<100642?i<100387?[2410,0]:[2411,0]:i<100659?[2558,0]:[2559,0]:i<101034?[2412,0]:[2560,0]:i<101245?[2706,0]:i<101498?[2854,0]:[2855,0]:[2413,0]:i<102028?i<101807?i<101779?[3002,0]:[3003,0]:[3150,0]:i<102197?[3298,0]:[3446,0]:i<104762?i<103121?i<102745?i<102537?i<102515?[3589,0]:[3590,0]:i<102582?[3591,0]:[3739,0]:i<102749?[3886,0]:i<102993?[3887,0]:[4035,0]:i<104135?i<103631?[3592,1]:i<103886?[3740,0]:[3741,0]:i<104548?i<104390?[3888,0]:[3889,0]:i<104734?[4036,0]:[4037,0]:i<104862?[3594,0]:[3742,0]:i<104934?[2419,0]:i<106699?i<105679?i<105417?i<105162?[2420,0]:[2421,0]:i<105450?[2568,0]:[2569,0]:[2422,2]:i<106718?[2717,0]:i<106807?[2718,0]:[2719,0]:i<118106?i<112553?i<110024?i<108778?i<107248?i<107127?i<107012?[1980,0]:[1981,0]:[1982,0]:[2128,7]:i<109004?i<108890?[1983,0]:[1984,0]:[2131,2]:i<110513?i<110268?i<110147?[1985,0]:[1986,0]:i<110385?[1987,0]:[1988,0]:[2133,9]:i<115972?i<114647?i<113117?i<112921?i<112725?[1989,0]:[1990,0]:[1991,0]:[2137,7]:i<114952?i<114818?[1992,0]:[1993,0]:[2140,2]:i<116102?i<116037?i<116021?[1994,0]:[1995,0]:i<116039?[1996,0]:[1997,0]:i<117119?i<116609?i<116357?[2142,0]:[2143,0]:[2290,1]:i<117596?i<117344?[2144,0]:[2145,0]:[2292,1]:i<127643?i<121120?i<119916?i<119636?[2424,7]:i<119816?i<119722?[2720,0]:[2721,0]:[2722,0]:i<120936?[2427,2]:i<121027?[2723,0]:[2724,0]:i<124824?i<123160?[2429,9]:i<123804?i<123529?i<123275?[2725,0]:[2726,0]:i<123550?[2873,0]:[2874,0]:[2727,2]:i<126441?i<125421?i<125079?i<124837?[3021,0]:[3022,0]:i<125166?[3169,0]:[3170,0]:[3023,2]:i<126825?i<126747?i<126495?[3317,0]:[3318,0]:[3466,0]:i<127335?[3319,1]:i<127492?[3467,0]:[3468,0]:i<145320?i<137593?i<132743?[2433,4]:i<135659?i<134273?[3025,7]:i<135151?i<134783?[3321,1]:i<134919?[3469,0]:[3470,0]:i<135406?[3323,0]:[3471,0]:i<136679?[3028,2]:i<137189?[3324,1]:i<137438?[3472,0]:[3473,0]:i<141673?[2438,12]:i<143713?[3030,9]:i<144370?i<144223?[3326,1]:i<144298?[3474,0]:[3475,0]:i<144880?[3328,1]:i<145065?[3476,0]:[3477,0]:i<145353?i<145341?i<145321?[3618,0]:[3619,0]:[3620,0]:i<146160?i<145789?i<145534?[3624,0]:[3625,0]:i<145905?[3772,0]:[3773,0]:i<146164?[3920,0]:[3921,0]:i<265018?i<198427?i<160066?i<153365?i<149393?i<148107?i<146577?i<146530?i<146432?[1998,0]:[1999,0]:[2000,0]:[2146,7]:i<148373?i<148217?[2001,0]:[2002,0]:[2149,2]:i<151724?i<150194?i<149884?i<149411?[1856,0]:i<149632?[2003,0]:[2004,0]:i<149939?[1857,0]:[2005,0]:[2151,7]:i<152345?i<151838?i<151799?[1858,0]:[1859,0]:i<152093?[2006,0]:[2007,0]:[2154,2]:i<156990?i<155458?i<153928?i<153666?i<153500?[2008,0]:[2009,0]:i<153680?[1862,0]:[2010,0]:[2156,7]:i<155970?i<155474?i<155469?[1863,0]:[1864,0]:i<155724?[2011,0]:[2012,0]:[2159,2]:i<158026?i<157491?i<156999?i<156995?[1865,0]:[1866,0]:i<157246?[2013,0]:[2014,0]:i<157522?i<157501?[1867,0]:[1868,0]:i<157772?[2015,0]:[2016,0]:[2161,9]:i<182787?i<177501?i<170266?[2442,5]:i<175366?[2447,4]:i<177124?i<176162?i<175978?i<175754?i<175606?[3039,0]:[3040,0]:[3187,0]:[3041,0]:i<177089?i<176580?i<176416?[3335,0]:[3336,0]:i<176835?[3483,0]:[3484,0]:i<177090?[3337,0]:[3485,0]:i<177313?[3042,0]:[3043,0]:i<181393?i<179798?i<179031?[3626,7]:i<179526?i<179510?i<179258?[3922,0]:[3923,0]:i<179511?[4070,0]:[4071,0]:i<179781?[3924,0]:[4072,0]:i<180818?[3629,2]:i<181325?i<181070?[3925,0]:[3926,0]:i<181338?[4073,0]:[4074,0]:i<182440?i<182393?i<181903?[3631,1]:i<182158?[3779,0]:[3780,0]:i<182433?[3633,0]:[3781,0]:i<182760?i<182685?[3927,0]:[3928,0]:[4075,0]:i<196516?i<188676?i<187886?i<185847?[2452,8]:i<186867?[2455,2]:i<187377?[2751,1]:i<187632?[2899,0]:[2900,0]:i<188453?i<188266?[3044,1]:[3046,0]:i<188613?[3047,0]:[3048,0]:i<192755?i<190716?[2457,9]:i<191735?i<191226?[2753,1]:i<191480?[2901,0]:[2902,0]:[2755,2]:i<194653?i<193633?i<193220?i<192965?[3049,0]:[3050,0]:i<193378?[3197,0]:[3198,0]:[3051,2]:i<195496?i<195070?i<194815?[3345,0]:[3346,0]:i<195241?[3493,0]:[3494,0]:[3347,2]:i<198114?i<197094?i<196891?i<196636?[3641,0]:[3642,0]:i<196901?[3789,0]:[3790,0]:[3643,2]:i<198124?[3938,0]:i<198243?[3939,0]:[3940,0]:i<211009?i<205338?i<202297?i<200776?i<199246?i<198981?i<198472?i<198451?[1869,0]:[1870,0]:i<198727?[2017,0]:[2018,0]:i<198995?[1871,0]:[2019,0]:[2165,7]:i<201277?i<200785?i<200780?[1872,0]:[1873,0]:i<201030?[2020,0]:[2021,0]:[2168,2]:i<203298?i<202800?i<202307?[1874,1]:i<202553?[2022,0]:[2023,0]:i<202808?[1876,1]:[2024,1]:[2170,9]:i<208992?i<207609?i<206079?i<205831?i<205345?i<205342?[1878,0]:[1879,0]:i<205589?[2026,0]:[2027,0]:i<205835?[1880,0]:[2028,0]:[2174,7]:i<207972?i<207610?[1881,0]:i<207825?[2029,0]:[2030,0]:[2177,2]:i<209048?i<209042?[2031,0]:[2034,0]:i<210034?i<209524?i<209301?[2179,0]:[2180,0]:[2327,1]:i<210499?i<210256?[2181,0]:[2182,0]:[2329,1]:i<237350?i<229366?i<221206?i<216109?[2461,4]:i<219166?i<217636?i<217126?i<216619?[3053,1]:i<216874?[3201,0]:[3202,0]:[3055,3]:[3349,7]:[3056,6]:[2466,11]:i<233439?i<231821?i<230896?[3645,7]:i<231520?i<231405?i<231150?[3941,0]:[3942,0]:i<231452?[4089,0]:[4090,0]:i<231775?[3943,0]:[4091,0]:i<232841?[3648,2]:i<233351?[3944,1]:[4092,1]:i<237232?i<235479?[3650,9]:i<236214?i<235989?[3946,1]:i<236036?[4094,0]:[4095,0]:i<236724?[3948,1]:i<236977?[4096,0]:[4097,0]:i<237233?[4243,0]:i<237268?[4244,0]:[4245,0]:i<255710?[2470,10]:i<261613?i<260810?[3654,4]:i<261203?i<261018?i<260910?[4246,0]:[4247,0]:[4248,0]:[4249,1]:i<264728?i<263563?i<262633?[3659,2]:i<263143?[3661,1]:i<263398?[3809,0]:[3810,0]:i<264577?i<264073?[3955,1]:i<264328?[4103,0]:[4104,0]:i<264704?i<264701?[3957,0]:[3958,0]:[4105,0]:i<264926?[4251,0]:[4252,0]:i<325349?i<277772?i<271363?i<268070?i<266773?i<265243?i<265149?i<265062?[2035,0]:[2036,0]:[2037,0]:[2183,7]:i<267050?i<266901?[2038,0]:[2039,0]:[2186,2]:i<270045?i<268515?i<268367?i<268218?[2040,0]:[2041,0]:[2042,0]:[2188,7]:i<270343?[2043,1]:[2191,2]:i<274861?i<273387?i<271857?i<271661?i<271513?[2045,0]:[2046,0]:[2047,0]:[2193,7]:i<273841?[2048,1]:[2196,2]:i<275732?i<275316?i<275088?[2050,0]:[2051,0]:i<275541?[2052,0]:[2053,0]:[2198,9]:i<300873?i<298008?i<287950?i<282850?i<280832?[2479,8]:i<281852?[2482,2]:i<282362?[2778,1]:i<282616?[2926,0]:[2927,0]:[3071,4]:i<292969?i<290929?i<289480?[2484,7]:i<290428?i<289990?[2780,1]:i<290244?[2928,0]:[2929,0]:i<290683?[2782,0]:[2930,0]:[2487,6]:i<295968?i<294499?[3076,7]:i<295507?i<295009?[3372,1]:i<295264?[3520,0]:[3521,0]:i<295762?[3374,0]:[3522,0]:[3079,6]:i<299493?i<298917?i<298613?i<298518?[3663,1]:i<298564?[3811,0]:[3812,0]:i<298868?[3665,0]:[3813,0]:i<299427?[3666,1]:[3814,1]:i<300855?i<299866?i<299855?i<299846?i<299737?[3668,0]:[3669,0]:[3816,0]:[3670,0]:i<300522?i<300279?i<300024?[3671,0]:[3672,0]:i<300289?[3819,0]:[3820,0]:i<300701?[3968,0]:[4116,0]:[4264,0]:i<316004?i<311073?[2489,5]:i<313937?i<312640?i<311734?i<311502?i<311305?[2494,0]:[2495,0]:i<311729?[2642,0]:[2643,0]:i<312234?i<311979?[2496,0]:[2497,0]:i<312385?[2644,0]:[2645,0]:i<313296?i<312948?i<312895?[2790,0]:[2791,0]:i<313203?[2938,0]:[2939,0]:i<313605?i<313350?[2792,0]:[2793,0]:i<313682?[2940,0]:[2941,0]:i<315146?i<314680?i<314308?i<314192?[3086,0]:[3087,0]:i<314563?[3234,0]:[3235,0]:i<314981?i<314727?[3088,0]:[3089,0]:i<314982?[3236,0]:[3237,0]:i<316000?i<315532?i<315401?[3382,0]:[3383,0]:i<315787?[3530,0]:[3531,0]:[3532,0]:i<322138?i<321104?[3673,4]:i<321700?i<321448?i<321250?[4265,0]:[4266,0]:i<321694?[4267,0]:[4415,0]:i<322135?i<321943?[4268,0]:[4269,0]:[4416,0]:i<325110?i<323576?i<323158?[3678,2]:i<323302?[3680,0]:i<323553?[3828,0]:[3829,0]:i<324596?[3974,2]:i<324891?i<324851?[3976,0]:[3977,0]:i<325105?[4124,0]:[4125,0]:i<325331?i<325239?[4270,0]:[4271,0]:[4272,0]:i<328003?i<327641?i<327102?i<325650?i<325632?i<325505?[2054,0]:[2055,0]:[2056,0]:i<326670?[2202,2]:i<326847?[2204,0]:[2352,0]:i<327131?i<327116?[2205,0]:[2206,0]:[2353,1]:i<327644?[2207,0]:i<327879?[2355,0]:[2356,0]:i<335868?i<333103?[2498,4]:i<334791?i<334632?i<334122?i<333613?[3090,1]:i<333867?[3238,0]:[3239,0]:[3092,3]:i<334746?i<334679?[3386,0]:[3387,0]:[3388,0]:i<335809?i<335301?[3093,1]:i<335556?[3241,0]:[3242,0]:i<335852?[3389,0]:[3390,0]:i<338652?i<337093?i<336887?i<336377?i<336123?[2503,0]:[2504,0]:[2651,1]:i<336947?[2505,0]:[2653,0]:i<338113?[2799,2]:i<338355?i<338346?[2801,0]:[2802,0]:i<338609?[2949,0]:[2950,0]:i<339584?i<339162?[3095,1]:i<339391?[3243,0]:[3244,0]:i<339798?i<339794?[3097,0]:[3098,0]:[3245,0];
			}

	}

	function Emitter_glimmerLadder() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-789.856,129.272],[-463.456,131.472],[-463.456,131.472]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 2;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 2;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder1() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-775.24,64.3599],[-463.84,64.5599],[-463.84,64.5599]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 1;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 1;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder2() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-763.568,0.103943],[-466.768,-0.0960693],[-466.768,-0.0960693]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 0.96;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 0.96;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder3() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-752.912,-65.808],[-463.312,-67.0081],[-463.312,-67.0081]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 1.02;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 1.02;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder4() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-737.84,-130.464],[-465.04,-130.464],[-465.04,-130.464]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 0.98;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 0.98;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder5() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-713.84,-195.648],[-466.24,-195.648],[-466.24,-195.648]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 1.01;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 1.01;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder6() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-702.368,-262.56],[-463.168,-262.56],[-463.168,-262.56]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 0.99;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 0.99;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	function Emitter_glimmerLadder7() {

		var _1, _2 = [], _2i, _2s = [], _3 = [], _5 = [], _7, _9, _11=[], _12, _13, _14, _15, _15i0, _15s0 = [], _16, _17, _17i0, _17s0 = [];
		this.pe = [{xe:0,Rc:1,Sc:1,renderStyleIndex:0}];
		this.name = "glimmerLadder";

		this.ud = function(Bd) {
			Bd.dd();
			Bd._2 = [
				[[-686.784,-325.488],[-429.584,-325.288],[-429.584,-325.288]]
			];
			Bd._15 = [
				[
					[0,8,8],
					[8,0,0]
				]
			];
			Bd._17 = [
				[
					[0,1,1],
					[1,0,0]
				]
			];
			Bd.jd = 100;
			Bd.Vc = 0;
		}

		this.Mb = function(vd) {
			vd.zb = 0.9;
			vd.Gb = 1;
			vd.Jb = 1;
		}

		this.Pb = function(Qb, Bd, vd) {
			vd.zb = 0.9;
		}

		this.fd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.gd = function(Bd, Xb) {
			Xb._ = 0.0;
			_1 = 0 + Bd.Ld.rand() * (1 - 0);
			_2i = Db.kb(_1);
			ctx.V(_2s,0,(_2i-0)*1);
			Db.lb(_2, Bd._2[_2s[0]], _2s[1]);
			ctx.W(_3, _2[0], _2[1], 0);
			Xb._4 = [];
			ctx.c(Xb._4, Bd.Ab, _3);
			ctx.randv3gen(_5, 7, Bd.Ld.rand);
			Xb._6 = [];
			ctx.T(Xb._6, _5);
			_7 = 0 + Bd.Ld.rand() * (360 - 0);
			Xb._8 = _7;
			_9 = 10 + Bd.Ld.rand() * (30 - 10);
			Xb._10 = _9;
			ctx.T(Xb.Ab, Xb._4);
		}

		this.qc = function(Qb, Bd, Xb) {
			Xb._ += Qb;
			ctx.T(_11, Xb._6);
			ctx.u(_11, _11, Qb);
			ctx.c(_11, _11, Xb._4);
			ctx.T(Xb._4, _11);
			_12 = Xb._8 + Qb * 33;
			Xb._8 = _12;
			ctx.T(Xb.Ab, Xb._4);
			_13 = 2;
			_14 = (Xb._ / _13);
			_15i0=(_14<0?0:(_14>1?1:_14));
			_15i0<0.204609?ctx.V(_15s0,0,(_15i0-0)*4.88738):ctx.V(_15s0,1,(_15i0-0.204609)*1.25724);
			_15 = Db.nb(Bd._15[0][_15s0[0]],_15s0[1]);
			_16 = (Xb._10 * _15);
			_17i0=(_14<0?0:(_14>1?1:_14));
			_17i0<0.2?ctx.V(_17s0,0,(_17i0-0)*5):ctx.V(_17s0,1,(_17i0-0.2)*1.25);
			_17 = Db.nb(Bd._17[0][_17s0[0]],_17s0[1]);
			ctx.S(Xb.Pd,[0.5,0.5]);
			Xb.Md = Xb._8;
			ctx.V(Xb.Nd,_16,_16);
			ctx.T(Xb.gf,[1,1,1]);
			Xb.Od = _17;
			Xb.Qc = 0;
		}

		this.Cc = function(Bd, Xb, Wc) {
			_13 = 2;
			if (Xb._ > _13) return true;
			return false;
		}


	}

	this.qd = function(Ld) {
		Ld.Dd = 0.0333333;
		Ld.Ud = 0;
		Ld.rand = function() { return Math.random(); };
		Ld.pd(new Emitter_glimmer());
		Ld.pd(new Emitter_glimmerLadder());
		Ld.pd(new Emitter_glimmerLadder1());
		Ld.pd(new Emitter_glimmerLadder2());
		Ld.pd(new Emitter_glimmerLadder3());
		Ld.pd(new Emitter_glimmerLadder4());
		Ld.pd(new Emitter_glimmerLadder5());
		Ld.pd(new Emitter_glimmerLadder6());
		Ld.pd(new Emitter_glimmerLadder7());
	}
		this.kb = function (v) { 			return (v < 0) ? 0 : ((v > 1) ? 1 : v); 		}

		this.lb = function (r, path, je) { 			var indexInt = Math.floor(je); 			var lerp = je - indexInt; 			ctx.Y(r, path[indexInt], path[indexInt + 1], lerp); 		}

		this.nb = function(funcValues, je) { 			var indexInt = Math.floor(je); 			var nextInt = indexInt + 1; 			return ctx.X(funcValues[indexInt], funcValues[nextInt], je - indexInt); 		}


}

return glimmerL;
}));